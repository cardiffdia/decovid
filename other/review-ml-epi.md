# Predicting Patient Outcomes following Surgery: A Literature Review

Academic literature [9] sets out a health care landscape in which AI can play an important role in administration, clinical decision support, patient monitoring and healthcare interventions. They frame Clinical decision support systems as computer programs that draw upon clinical data and knowledge to support decisions made by healthcare professionals and highlight that AI can help clinicians understand the risks associated with treatments leveraging large data sets. Eagle and Eschima [4] focus down on the opportunities for new tools to improve pre-operative risk assessment, highlighting that referring physicians and consultants face a persistent challenge in the process of pre-operative clinical risk assessment. In the editorial article they show numerous examples of statistical tooling which could be used to better understand the risks faced by patients undergoing surgical procedures, but highlight that there are weaknesses in the specific risk evaluation tools commonly used and the potential for clinical decision making to be prejudiced by adherence to a single tool's *decision*.

In this data science theme, we provide an overview of current research on machine learning for pre-operative surgical risk assessment and prediction. We identified the need for a review of current machine learning methods, risk prediction tools, influential risk factors and open public data sets available in terms of predicting patient outcomes following surgery.

## Literature Review

This review references a range of data science techniques you may or may not be familiar with. Please feel free to contact the DIA if you would like us to elaborate on any of the techniques. Table 1 provides a list of relevant resources, journals, conferences and workshops which we believe contain publications relevant to the company's objectives. We bound the search using a simple set of criteria:

*  must have been published in 2018 or 2019
*  must have a sample size of 1000 patients or greater
*  must be related to novel statistical or machine learning approaches to predicting post-operative risks or
*  must be related to adults

| Type  | Name |  URL  |
|---|---|---|
| Library  | Cardiff University Library  | bit.ly/dia-cardiff-libraries |
| Library  | Healthcare Resources | bit.ly/dia-healthcare-resources  |
| Institute | National Institutes of Health (NIH) | bit.ly/dia-nih |
| Repository | re3data |  bit.ly/dia-re3data |
| Repository  | Global Health Data Exchange | bit.ly/dia-global-data-health-exchange  |
| Repository  | FAIRSharing |  bit.ly/dia-fairsharing |
| Repository  | US National Library of Medicine National Institutes of Health |  bit.ly/dia-nlm-nih |
| Repository  | PubMed | bit.ly/dia-pubmed  |
| Repository  | MEDLINE Journals | bit.ly/dia-medline-journals  |
| Repository  | NICE | bit.ly/dia-nice  |
| Journal  | Journal of Medical Internet Research  | bit.ly/dia-jmir  |
| Journal  | British Medical Journal  | bit.ly/dia-bmj  |
| Journal  | The Lancet  | bit.ly/dia-lancet  |
| Journal  | Nature  | bit.ly/dia-nature  |
| Journal  | Annals of Internal Medicine  | bit.ly/dia-annals-internal-medicine |
| Journal  | Journal of the American Medical Association  | bit.ly/dia-jama  |
| Journal  | Data in Brief  | bit.ly/dia-data-in-brief |
| Journal  | Scientific Data  | bit.ly/dia-scientific-data |
| Journal  | Data  | bit.ly/dia-mdpi |
| Journal  | Open Health Data  | bit.ly/dia-open-health-data |
| Conference  | Ai4 Healthcare  | bit.ly/dia-ai4-healthcare  |
| Conference  | DSHealth | bit.ly/dia-dshealth  |
| Conference  | epiDAMIK | bit.ly/dia-epidamik |
| Conference  | Machine Learning for Healthcare (MLH)  | bit.ly/dia-mlh  |
| Workshop  | Machine Learning for Medicine and Healthcare (MLMH)  | bit.ly/dia-mlmh |
| Workshop  | Knowledge Discovery in Healthcare Data (KDH)  | bit.ly/dia-kdh  |
| Workshop  | AI in Health (AIH)  | bit.ly/dia-ai-health  |

*  Table 1: List of relevant resources, journals, conferences and workshops

### Synopsis of accepted papers

In this section we present a brief synopsis of each paper accepted for review.

#### Predicting Surgical Complications in Patients Undergoing Elective Adult Spinal Deformity Procedures Using Machine Learning

[1] explores the application of logistic regression (LR) and artificial neural networks (ANN) for risk factor analysis in orthopaedic surgery using sex, age, ethnicity, diabetes, smoking, steroid use, coagulopathy, functional status, American Society of Anesthesiologists (ASA) class O3, body mass index (BMI), pulmonary comorbidities, and cardiac comorbidities. The study of 5,818 patient records in the NSQIP database finds that both LR and ANN outperform the ASA scoring in predicting every complication ($p<.05$). For cardiac complication LR achieved AUC of 0.69, ANN 0.76 and ASA score just 0.47. For VTE, LR achieved 0.55, ANN 0.54 and ASA 0.49. For wound complication, LR achieved 0.58, ANN 0.61 and ASA 0.51. For mortality, LR achieved 0.79, ANN 0.84 and ASA score just 0.52.

#### An Automated Machine Learning-Based Model Predicts Postoperative Mortality using Readily-Extractable Preoperative Electronic Health Record Data

[2] report on the use of random forest classifiers to predict postoperative in-hospital mortality based on data available at the time of surgery. The study includes 53,097 electronic health records of patients that underwent anaesthesia between 2013 and 2018. The models including basic patient information such as age, sex, BMI, BP, and HR; laboratory tests frequently obtained before surgery, such as sodium, potassium, creatinine, and blood cell counts; and surgery-specific information such as the surgical procedure codes. In total, 58 preoperative features (including ASA status) were used. The study finds that the random forest method out performs well, achieving AUC of 0.932. Baselines for comparison of performance are provided, showing that the Preoperative Score to Postoperative Mortality (POSPOM) score achieves 0.66 and Charlson comorbidity scores achieves AUC 0.74.

#### Machine Learning for Real-Time Prediction of Complications in Critical Care: A Retrospective Study

[3] applies a recurrent deep neural network (RNN) to the MIMIC-III dataset for a heart surgery cohort between 2000 and 2016 including 42,007 patients. 52 patient features were examined and split into patient characteristics (4 features), surgery (9), vital signs (11), arterial blood gas (9), laboratory results (17) and balance output (2).  Three outcomes were analysed (bleeding, mortality and renal failure) with post-op bleeding more difficult to predict than the other two outcomes.  The predictive performance of the model was assessed via a number of performance metrics such as positive predictive value, negative predictive value, sensitivity, specificity, and area under the curve.  Recurrent neural network predictions resulted in statistically significantly higher accuracy than those of the clinical reference tools examined (Simplified Acute Physiology Score (SAPS)and Kidney Disease Improving Global Outcomes (KDIGO)), absolute accuracy improvement in bleeding of 0·22, p=0·0007; absolute accuracy improvement in mortality of 0·17, p=0·0060; absolute accuracy improvement in renal failure of 0·17, p=0·0008), determined by a Wilcoxon signed-rank test.  In all models, the area under the curve was greater than 0.87 indicating high predictive performance.

#### A Machine Learning Approach for Predictive Models of Adverse Events following Spine Surgery

[5] applied a least absolute shrinkage and selection operator (LASSO) regularisation method and a logistic regression approach for predicting the risks of an overall adverse events and the top six most commonly observed adverse events in the 30-day postoperative window. Predictors included patient demographics, location of the spine procedure, comorbidities, type of surgery performed, and preoperative diagnosis. The study found that LASSO and logistic regression predicted Pulmonary complication with AUC 0.75, Congestive heart failure with AUC 0.75, Pneumonia AUC 0.74, Urinary tract infections AUC 0.71, Neurologic complication AUC 0.69, Cardiac dysrhythmia AUC 0.72, Overall adverse event AUC 0.7, Overall medical complication AUC 0.7 and Overall surgical complication with AUC 0.69. The study does not provide comparison to other predictive measure or standard instruments, such as CCI, ECI or CMS-HCC.

#### Surgical Risk Is Not Linear: Derivation and Validation of a Novel, User-Friendly, and Machine-learning-based Predictive Optimal Trees in Emergency Surgery Risk (POTTER) Calculator

[6] introduces Predictive OpTimal Trees in Emergency Surgery Risk (POTTER). Optimal Classification Trees (OCT), which were leveraged to train the machine-learning algorithm to predict postoperative mortality, morbidity, and 18 specific complications (e.g. sepsis, surgical site infection) based on ACS-NSQIP electronic health records. The Performance of Optimal Classification Trees (OCT) in the Predicting 30-day Postoperative outcomes was favourable, as Compared with other known risk-prediction models, achieving AUC 0.91 - 0.92 for mortality and AUC 0.83 - 0.84 for morbidity. Predictive accuracy for eighteen specific postoperative complications are evaluated, achieving between AUC 0.68 - 0.93. Of particular note in this study was the development of the POTTER interface, as shown in Figure X. AUC values ranging between 0.82 and 0.94. The model predicts the risk for death at 1, 3, 6, 12, and 24 months with AUC values ranging between 0.77 and 0.83.

#### MySurgeryRisk: Development and Validation of a Machine-learning Risk Algorithm for Major Complications and Death After Surgery

[7] introduces MySurgeryRisk, a real-time probabilistic risk scoring system for 8 major postoperative complications (acute kidney injury, sepsis, venous thromboembolism, intensive care unit admission>48 hours, mechanical ventilation >48 hours, wound, neurologic, and cardiovascular complications) and death up to 24 months after surgery. The system uses a combination of EHR, sensor data, omics, video and image data, and public data sets to both predict outcomes using a combination of generalised additive models (GAM) and random forest classifiers (RF). MySurgeryRisk is one of more sophisticated systems reviewed, as shown in Figure XX, the system uses a combination of real-time streaming and active learning to improve predictions via a high performance computing platform, and incorporates data privacy layers on both ingest and output.

#### Machine-Learning identifies Best Measures to Predict ACL Reconstruction Outcome

Knee injury and Osteoarthritis Outcome Score (KOOS) is a widely used patient-reported outcome measurement to track recovery after ACL surgery. [8] uses random forest and gradient boosting to identify the most critical survey questions that predict KOOS ADL scores with high accuracy. The study uses PROMs data obtained from the Surgical Outcome System data registry (SOS). Results from the ML models indicated that by 6 key questions, over 80\% of the variance in KOOS ADL scores could be explained instead of standard 17 survey questions.

#### Predicting Patient-Reported Outcomes following Hip and Knee Replacement Surgery using Supervised Machine Learning

Machine-learning classifiers mostly offer good predictive performance and are increasingly used to support shared decision-making in clinical practice. Focusing on performance and practicability, the study presented in [10] uses NHS PROMs data (130,945 observations) to evaluate prediction of patient-reported outcomes measures (PROMs) by eight supervised classifiers, following hip and knee replacement surgery. The area under the receiver operating characteristic of the best training models for hip replacement achieves ~0.87 for VAS and ~0.78 for Q score, and ~0.86 on VAS and ~0.70 on Q score for knee replacement surgery. They conclude that Preoperative VAS, Q score and specific dimensions like limping are the most important predictors for postoperative hip and knee PROMs.

#### Can We Improve Prediction of Adverse Surgical Outcomes? Development of a Surgical Complexity Score Using a Novel Machine Learning Technique

[11] introduce a new surgical Complexity score based on ~one million Medicare Inpatient and Outpatient Standard Analytic Files (SAFs). The complexity score uses a stochastic hill-climbing algorithm to learn how to weight comorbidities to optimise the area under the curve (AUC) for predicting adverse outcomes for a broad range of common elective surgeries. In the study, the new Complexity score is used to predict adverse outcomes for Colectomy, Abdominal aortic aneurysm, Coronary artery bypass graft, Hip arthroplasty, Knee arthroplasty and Lung resection surgeries. Post-operative outcomes include 90-day readmission, 30-day readmission, 90-day mortality, 30-day mortality and postoperative super-use. The Complexity score modestly out performed Charlson Comorbidity Index (CCI), Elixhauser Comorbidity Index (ECI), and the Centers for Medicare and Medicaid Service`s Hierarchical Condition Category (CMS-HCC) on 'any complication at index' (AUC=0.867), 90-day readmission (AUC=0.706), 30-day readmission (AUC=0.716) and postoperative super-use (AUC=0.817). CMS-HCC performed modestly better than other measures for 30-day (AUC=0.872) and 90-day mortality (AUC=0.872).

#### Utilizing Machine Learning Methods for Preoperative Prediction of Postsurgical Mortality and Intensive Care Unit Admission

[12] evaluate four machine learning algorithms to predict postsurgical 30-day mortality and ICU stays over 24 hours, using data from the Singapore General Hospital (SGH) between January 1, 2012 and October 31, 2016. Patient demographics included age, sex, ethnicity, height, weight, and body mass index (BMI) in addition to comorbidities, kidney and renal function, and preoperative lab tests amongst other features.

For mortality prediction, the study found Random forest (RF) achieved AUROC 0.96, Adaptive boosting (ADA) achieved AUROC 0.95, Gradient boosting (GB) achieved AUROC 0.96 and Support vector machine (SVM) achieved AUROC 0.94. Baseline models CARES achieved AUROC 0.94 and ASA-PS 0.89. For prediction of ICU stays over 24 hours, Random forest (RF) achieved AUROC 0.95, Adaptive boosting (ADA) AUROC 0.94, Gradient boosting (GB) AUROC 0.95 and Support vector machine (SVM) 0.94. Baseline models CARES achieved AUROC 0.84 and ASA-PS 0.80. The study concludes that overall, gradient boosting provided the best performance with AUPRCs of 0.23 and 0.38 for mortality and ICU admission outcomes respectively.

#### Ensemble-Based Classification Models for Predicting Post-Operative Mortality Risk in Coronary Artery Disease

[13] presents findings of a study of 4,908 patients who underwent isolated CABG during the study period, mortality estimates of 30-day and 1-year post CABG surgery were 1.59% and 3.85%, respectively. Descriptive analysis revealed that age, sex, hypertension, dialysis, cerebrovascular disease, chronic obstructive pulmonary disease, and chronic heart failure were associated with 30-day and 1-year mortality. The accuracy of the LR and RF regression classifiers in predicting 30-day mortality were 74.1, and 99.7\%, respectively. While the accuracy of the former and latter classifiers in predicting 1-year post CABG mortality were 74% and 97.4%, respectively.

#### Use of Machine Learning for Prediction of Patient Risk of Postoperative Complications after Liver, Pancreatic, and Colorectal Surgery

[14] used decision tree models to predict the occurrence of any complication, as well as specific complications for 15,657 patients in the ACS-NSQIP database undergoing liver, pancreatic and colorectal surgery from 2014 to 2016. Information in the final dataset included preoperative comorbidities and peri-operative clinical variables, as well as 30-day postoperative complications and mortality.

The algorithm achieved C-statistic of 0.74 for 'any complication', out performing the ASA (C-statistic 0.58) and ACS-Surgical Risk Calculator (C-statistic 0.71). The algorithm was able to predict with high accuracy thirteen out of the seventeen complications analysed. The best performance was in the prediction of stroke (C-statistic 0.98), followed by wound dehiscence, cardiac arrest, and progressive renal failure (all C-statistic 0.96). The algorithm had a good predictive ability for superficial SSI (C-statistic 0.76), organ space SSI (C-statistic 0.76), sepsis (C-statistic 0.79), and bleeding requiring transfusion (C-statistic 0.79).

#### Machine Learning Algorithm Identifies Patients at High Risk for Early Complications after Intracranial Tumor Surgery: Registry-Based Cohort Study

[15] presents a study in which patients from an ongoing prospective patient registry at a single tertiary care centre with an intracranial tumour that underwent elective neurosurgery between June 2015 and May 2017. Gradient boosting machine learning algorithms provided the model best predicting the probability of an EPC. The model scored an accuracy of 0.70 (confidence interval [CI] 0.59-0.79) with an area under the curve (AUC) of 0.73 and a sensitivity and specificity of 0.80 (CI 0.58-0.91) and 0.67 (CI 0.53-0.77) on the test set. The conventional statistical model showed inferior predictive power (test set: accuracy: 0.59 (CI 0.47-0.71); AUC: 0.64; sensitivity: 0.76 (CI 0.64-0.85); specificity: 0.53 (CI 0.41-0.64)).

#### A Review of Recent Advances in Data Analytics for Post-Operative Patient Deterioration Detection

[16] provide a review of recent advances in data analysis for post-operative patient deterioration and early warning scores. [16] posit that despite considerable advances in anesthesia, ventilation, surgical and monitoring technologies, about 25\% of surgical patients suffer from postoperative complications. Serious adverse events in the time following surgery include respiratory failure, cardiac arrest, severe sepsis, and acute renal failure. These conditions constitute life-threatening risks, in particular for elderly patients and emergency surgery patients.

The review focuses on detecting deterioration after surgery, so is not directly relevant to the task of predicting pre-operatively which outcomes are most likely. However, [16] provide a useful table of features and scores used to predict postoperative deterioration, grouped by peri-operative stage. This table has been recreated in Table XX.

#### Perioperative Risk Factors for Postoperative Respiratory Failure

[17] identifies risk factors for respiratory failure after surgery. 4055 patients were included in the analysis of which 98 were identified with Post Respiratory Failure (PRF). Independent risk factors were older age, inpatient status, hypertension, COPD, elective procedure, surgical duration >2 hours, and ASA class >=3.  Multivariate logistic regression analysis of signiﬁcant variables found patient age, body mass index (BMI), surgical duration, elective case, in-patient status, and the preoperative diagnosis of COPD to be signiﬁcant risk factors for PRF.

#### Preoperative Risk Index Among Patients Undergoing Thyroid or Parathyroid Surgery

[18] compares preoperative risk indices to determine those factors associated with short term major postoperative adverse events (or death) in patients undergoing thyroid or parathyroid surgery. 154,895 patients who underwent these surgeries between 2007 and 2016 were analysed.  Preoperative variables were categorised into sociodemographic, clinical, and frailty-related risk factors. A new Cervical Endocrine Surgery Risk Index (CESRI) was compared with existing risk models.  Multiple logistic regression analyses was performed with C statistics.  The CESRI outperformed other risk models for determining adverse events. The area under the curve of the CESRI to determine major adverse events, including death, using the validation cohort was 0.63 (95% CI, 0.61-0.64), with a sensitivity of 0.66 (95% CI, 0.64-0.68) and specificity of 0.66 (95% CI, 0.65-0.66).  CESRI vs 5-Factor Modified Frailty Index: delta C index, 0.11; 95% CI, 0.09-0.13; CESRI vs American Society of Anesthesiologists Physical Status Classification System: delta C index, 0.05; 95% CI, 0.03-0.07; CESRI vs American College of Surgeons Risk Calculator: delta C index, 0.02; 95% CI, 0.01-0.03; and CESRI vs Head and Neck Surgery Risk Index: delta C index, 0.04; 95% CI, 0.03-0.06).

#### Association of Frailty With 30-Day Outcomes for Acute Myocardial Infarction, Heart Failure and Pneumonia Among Elderly Adults

[19] examines whether adding frailty measures to comorbidity-based risk adjustment models improves predicting patient outcomes for acute myocardial infarction, heart failure and pneumonia. Those aged over 65 years in the US in 2016 of Medicare fee-for-service beneficiaries were included in the study (785,127 participants). The incremental effect of adding the hospital frailty risk score (HFRS) to current comorbidity-based risk adjustment models was evaluated. A HFRS more than 15 (compared with an HFRS < 5) was associated with a higher risk of 30-day post-admission mortality (adjusted odds ratio (AOR), 3.6; 95% CI, 3.4-3.8), 30-day post discharge mortality (AOR, 4.0; 95% CI, 3.7-4.3) and 30-day readmission (AOR, 3.0; 95% CI, 2.9-3.1) after multivariable adjustment. Similar results were seen in the other two outcomes. By adding HFRS to comorbidity-based prediction models, an improvement in the predicted outcome was apparent in all three conditions.

#### Development and Validation of Outcome Prediction Models for Aneurysmal Subarachnoid Haemorrhage: the SAHIT Multinational Cohort Study

[20] examined 10,936 patients from seven randomised controlled trials and two prospective observational hospital registries to develop and validate a prediction tool for the outcome of subarachnoid haemorrhage (SAH) from ruptures intracranial aneurysms.  A validation set of 3,355 patients were used.  The outcome measure was the Glasgow scale at three months (a five-category scale from 1(dead) to 5 (good recovery)).  Proportional odds logistic regression models were used to determine the association between predictor variables and the Glasgow scale.  The core model included patient age, premorbid hypertension, and neurological grade on admission to predict the risk of functional outcome with the area under the receiver operator characteristics curve of 0.80 (95%CI 0.78 to 0.82) increasing to 0.81 (0.79-0.84) with inclusion of clot volume, aneurysm size and location.  Adding in treatment modality produced a similar AUC. The discrimination was lower when predicting the risk of mortality at 0.76 (0.69-0.82).

#### Is it Time to include Cancer in Cardiovascular Risk Prediction Tools?

[21] examines the need to include cancer indicators in cardiovascular risk prediction tools and advise that clinicians should account for cancer, especially chemotherapy as a risk factor for cardiovascular disease.

#### Development and Validation of a Risk-Prediction Model for In-Hospital Major Adverse Cardiovascular Events in Patients Admitted to Hospital with Acute Myocardial Infarction: Results from China PEACE Retrospective and Prospective Studies

[22] develops a prediction tool based on 4,485 patients throughout mainland China from a prospective study in 2012 to 2014 with 11,476 patients from a national retrospective study in 2015 used for external validation. Patient characteristics, medical histories, risk factors at admission and in-hospital events were abstracted from medical records with the primary outcome being major adverse cardiovascular event (MACE) including death, recurrent acute myocardial infarction (AMI) or ischaemic stroke.  Step-wise logistic regression and Markov chain Monte Carlo simulation methods were used.  Nine predictors were independently associated with in-hospital MACE with the model performing well in discrimination and calibration capability (C statistic of 0.85, 0.74 and 0.80) in the derived samples and two validation samples.

#### Risk Stratification Tools in Emergency General Surgery

[23] examines 13 risk stratification tools, particularly concerned with emergency general surgery for patients' perioperative risk for death or complication.  Six criteria were used to evaluate the tools including quantifying morbidity and mortality risk, using objective data, use in clinical practice and applicability in non-operable cases. Tools included American Society of Anaesthesiologists Physical Status (ASA-PS) Classification System, American College of Surgeons-National Surgical Quality Improvement Program (ACS-NSQIP), Charlson Comorbidity Index (CCI), Emergency Surgery Acuity Score (ESAS), Physical ESAS (PESAS), Physiological and Operative Severity Score for the Enumeration of Mortality and Morbidity (POSSUM), Perioperative Mortality Risk Score (PMRS), Surgical Apgar Score (SAS),  Surgical Mortality Score (SMS), Surgical Outcome Risk Tool (SORT), Surgical Risk Scale, Surgical Risk Score, Track and Trigger System.  The evaluation concluded that the NSQIP surgical risk calculator to be used in the emergency general surgery population but ESAS should also be considered for use.

#### Development and Validation of Machine Learning Models for Prediction of 1-Year Mortality Utilizing Electronic Medical Record Data Available at the End of Hospitalization in Multicondition Patients: a Proof-of-Concept Study

[24] applies logistic regression and random forest models to 59,848 patients within six hospital networks in Minnesota between 2012 and 2016. A large number of features were included including demographic variables, physiologic variables, biochemical variables and clinical comorbidity variables. Random forest models generally performed better than logistic regression models (AUC greater than 0.9 with all four classes of variables for random forest).  25 of the 27 top features were physiologic, laboratory and demographic features. The random forest model slightly overestimated the probability of death within 1 year when the probability of death was low, but the difference was always less than 10%.

#### Associations between Post-Operative Rehabilitation of Hip Fracture and Outcomes: National Database Analysis

[25] examines 17,708 patients with rehabilitation treatment and 30-day mobility data and 34,142 patients with rehabilitation treatment and discharge destination data using the English National Hip Fracture Database (NHFD) from 191 English hospitals treating hip fractures.  An intervention was defined as early mobilisation rehabilitation treatments by a physiotherapist.  Ordinal logistic and propensity scoring regression models were used to adjust for age, sex, pre-fracture mobility (1 to 5), operative delay and cognitive function and peri-operative risk score. The mobilisation-propensity score predicts mobilisation, with a Somers’ D statistic of 0.298. The average treatment effect adjusted scenario means are a little closer together than the unadjusted scenario means. The outcome is better (lower mobility score) in mobilised patients than in immobilised patients, whether the mobilisation is done by a PT or by somebody else, and the difference persists after propensity weighting.

#### Determinants of Perioperative Outcome in Frail Older Patients

[26] examines determinants of perioperative outcome in frail older patients and explores the existing instruments such as those developed by Fried, Edmonton, Longitudinal Urban Cohort Age Study (LUCAS) Functional Index and Manageable Geriatric Assessment (MAGIC).  Many definitions only include physical aspects while others include cognitive, emotional and social factors. The definition of frailty needs to be standardised with the increasing proportion of elderly patients.  A review article including 23 studies found that frailty was the strongest predictor of 90 day mortality with an odds ratio (OR) of 10.4 (95%CI: 7.6-14.2), p<0.001 and 1 year mortality OR of 8.4 (95%CI:6.4-11.1), p<0.001.

#### Risk Prediction Tools to Improve Patient Selection for Carotid Endarterectomy Among Patients with Asymptomatic Carotid Stenosis

[27] develops a risk prediction tool to improve patient selection for Carotid Endarterectomy (CEA) among patients with asymptomatic carotid stenosis.  2,325 patients who received CEA and carotid imaging between 2005 and 2009 were followed up for five years.  A risk prediction tool based on 23 variables identified in literature was developed using the Veterans Administration and Medicare data.  The area under the curve (AUC) was assessed for model performance. The mean age was 73.7 years and predominantly male and white ethnicity. Nearly 30% of patients died within 5 years of CEA. A backward selection algorithm was used, and 9 patient characteristics selected for the final logistic model, which yielded an optimism-corrected AUC of 0.687 for the Carotid Mortality Index.  A simpler model was also developed based on four comorbidities with an AUC of 0.657.  The use of these tools may improve patient selection for CEA patients with asymptomatic carotid stenosis.

#### The Incremental Predictive Value of Frailty Measures in Elderly Patients undergoing Cardiac Surgery: A Systematic Review

[28] is a systematic review of twelve studies, however, most of these studies contain less than 1000 patients.  The review concludes that frailty is a powerful indicator in patients undergoing cardiac surgery and improves the model performance with respect to predicting mortality but whether to adopt this in routine clinical practice is still unclear.

#### Prognostic Models for Outcome Prediction in Patients with Chronic Obstructive Pulmonary Disease: Systematic Review and Critical Appraisal

[29] is a systematic review of 228 articles involving 408 prognostic models in COPD patients and 20 prognostic models for diseases other than COPD.  The most commonly used predictors were age (166 models), forced expiratory volume in one second (85), sex (74), body mass index (66) and smoking (65).  However, only 7 of the models examined were identified with an overall low risk of bias using PROBAST, an assessment tool designed for systematic reviews of diagnostic or prognostic prediction models. A number of modelling techniques were performed throughout the articles including Cox regression (90), logistic regression (79), negative binomial regression (21), linear regression (16) and non-linear models (17).  Findings indicate a number of methodological pitfalls and a low rate of external validation for many of the articles.  Research should focus on existing models through update and external validation as well as the assessment of safety, clinical effectiveness and cost effectiveness.

#### Machine Learning–Based Preoperative Predictive Analytics for Lumbar Spinal Stenosis

[30] highlights that PROMS (Numeric Rating Scale NRS), NRS for Back Pain (NRS-BP), NRS for Leg Pain (NRS-LP) and Oswestry Disability Index (ODI)) for spinal decompression surgery show heterogeneity and posit that personalisation could provide more informative information for decision making. The paper evaluates seven machine learning methods to predict "clinically relevant end-points" based on preoperative data. ML techniques include extreme gradient boosting (XGBoost), Bayesian generalised linear models (GLMs), boosted trees, random forest, simple GLMs and shallow neural networks. They find that at 6-weeks postoperative ML models were able to predict improvement in PROMS with 69% (ODI, Bayesian GLM), 76% (NRS-LP, neural network & random forest) and 85% (NRS-BP, boosted trees, XGBoost). At 12-months postoperative ML models predict clinically meaningful improvement with 66% (ODI), 63% (NRS-LP) and 51% (NRS-BP) accuracy. They found that XGBoost was able to predict extended hospital stays with an accuracy of 77%. Figure XXX shows a table giving the importance of variables in predicting PROMs outcomes based on the AUC of ML-based models.

### Discussion

Table 2 presents the frequency with which specific machine learning techniques were used in the reviewed papers, and if applicable, which PROMs they were used to predict. The review finds that *Logistic Regression* is the most commonly used technique, with *Random Forest*, *Gradient Boosting*, *Artificial Neural Networks* and *Decision Trees* also used in multiple reviewed papers.


| Technique  | PROM  | Citation |
|---|---|---|
| Artificial Neural Network  | NRS-LP, EQ-5D-3L VAS, OHKS (Q-score)  | [30, 1, 10, 3]  |
| Decision tree  | KOOS  |  [6, 8, 14] |
| Bayesian GLM  | ODI | [30] |
| Gradient Boosting  | NRS-BP, KOOS, EQ-5D-3L VAS, OHKS (Q-score) | [30, 8, 10, 12, 15] |
| Adaptive Boosting  |  | [12] |
| Support Vector Machine  |  | [12] |
| Logistic Regression |  | [1, 5, 13, 17, 18, 19, 20, 22, 24, 25, 27, 29] |
| Stochastic hill-climbing |  | [11] |
| LASSO |  | [5, 29] |
| GAM |  | [7] |
| Random Forest | KOOS, EQ-5D-3L VAS, OHKS (Q-score) | [7, 8, 10, 12, 13, 24] |
| Markov chain Monte Carlo simulation |  | [22] |
| Restricted cubic splines |  | [29] |

*  Table 2: Machine learning techniques

Note that techniques in papers regarding systematic reviews are not included in the above table

While most of the reviewed papers focus on predicting post-operative mortality, morbidity or surgical complications, a number of papers address prediction of PROMs directly. Promps predicted include NRS-LP, EQ-5D-3L (VAS), Oxford Hip and Knee Score (OHKS Q-Score) and Knee Injury and Osteoarthritis Outcome Score (KOOS), ODI and NRS-BP. Table 3 shows which PROMs have been predicted using Machine Learning techniques and full details of results can be found in the *Synopsis of accepted papers* section.

Currently, the company do not have data available, however, are soon to start rolling out their electronic consent tool to four trusts in England and Wales and will begin to acquire this data.  The literature review identified a number of public data sets that are available to use and are found in Table 3.


|Feature	|				Citation|
|---|---|
|Sex			|		[1, 2, 11, 5, 12, 13, 15, 18, 19, 22, 25, 29]|
|Age			|		[1, 2, 11, 5, 12, 13, 14, 15, 17, 18, 19, 20, 22, 24, 25, 26, 27, 29]|
|Ethnicity		|		[1, 11, 12]|
|Height			|		[12, 15]|
|Functional status1	|		[1,14]|
|Diabetes		|		[1, 11, 5, 12, 14, 27]|
|Smoking	|			[1, 14, 15, 18, 29]|
|Alcohol	|				[15]|
|Steroid use	|			[1, 14]|
|Coagulopathy	|			[1, 18]|
|American Society of Anesthesiologists (ASA) classification			|	[1, 25]|
|Weight, BMI or obesity	|		[1, 2, 11, 12, 15, 17, 18, 24, 27, 29]|
|Pulmonary disorders or comorbidities|	[1, 11, 5, 12, 13, 14, 15, 19, 24, 27]|
|Cardiac disorders or comorbidities|	[1, 11, 12, 13, 14, 15, 19, 22, 27]|
|Lab tests2		|		[2, 12, 14, 15, 18, 22, 24, 28]|
|Renal			|		[11, 12, 14, 22, 27]|
|AIDS, Infarction, Dementia, Liver Disease, Tumour, Acute kidney failure, Diverticulosis, Rheum & Anxiety, Fibromyalgia, Hypothyroidism, Hyperlipidaemia |		[11]|
|Plegia or bleeding	|		[11, 14]|
|Malignancy		|		[11, 21, 24, 27]|
|Anaemia		|		[11, 18]|
|Depression	|			[11, 28]|
|Dialysis	|				[13, 14]|
|Hypertension	|			[11, 13, 14]|
|Surgical details or procedure codes |	[2, 12, 15, 20]|
|Dyspnea3	|			[14, 18]|
|Tumour size and nodal status	|			[14]|
|Pathology4	|			[15]|
|Surgical duration|			[17, 18]|
|Inpatient status|				[17, 18]|
|Prognostic grade|			[20]|
|Length of stay	|			[24]|
|Surgery delay|				[25]|
|Cognitive function|			[25, 28]|
|Walk test and Exhaustion|				[28]|

1 *including Independent, Partially Dependent and Totally Dependent*

2 *including Heart Rate,  Blood pressure,  Sodium,  Potassium,  Creatinine, Blood Cell Count, Haemoglobin*

3 *including At Rest, Moderate exertion, No*

4 *including Suspected Diagnosis and Location*

* Table 3: Identified model features

- Note that features in papers regarding systematic reviews are not included in the above table

For example, the Medical Information Mart for Intensive Care III (MIMIC-III) is an open public database comprising of over 40,000 anonymised patients that stayed in critical care units of the Beth Israel Deaconess Medical Center,  Boston, Massachusetts between 2001 and 2012. The database includes information such as demographics, vital sign measurements, laboratory test results, procedures, medications, imaging reports, and mortality (both in and out of hospital).

The Patient Episode Database for Wales (PEDW) was implemented in April 1991 and contains all inpatient and day case activity undertaken in NHS Wales plus data on Welsh residents treated in English Trusts. Data sets are freely available and request and analysis service is also provided. Further information can be found at [here](https://bit.ly/dia-pedw)

Table 4 identifies the features included in the best model. As expected, those features identified to strongly predict patient outcomes in the majority of papers were age, sex, weight/BMI/obesity, pulmonary disorders/comorbidities, cardiac disorders/comorbidities and laboratory tests (including heart rate, blood pressure, sodium, potassium, creatinine, blood cell count, haemoglobin) followed by smoking and diabetes.


| Data set | Citation | URL |
|---|---|---|
| Medical Information Mart for Intensive Care III (MIMIC-III)  | https://bit.ly/dia-mimic  | [3]  |
| ACS National Surgical Quality Improvement Program (ACS-NSQIP)  |  https://bit.ly/dia-acs-nsqip | [1, 14] |
| English National Hip Fracture Database (NHFD) |  https://bit.ly/dia-nhfd |  [25] |
| Surgical Outcome System data registry (SOS) | https://bit.ly/dia-surgical-outcome | [8] |
| NHS PROMs | bit.ly/dia-nhs-proms | [10] |
| Veterans Administration and Medicare data  | https://bit.ly/dia-veterans  |  [27] |

- Table 4: Available data sets to use

From the papers identified here, it is clear that some predictors are more important for specific patient outcomes. Table 2 tries to capture the frequency with which features are included in models. Many of the papers rank the importance of the features used, as shown in Figure 3. However, the review highlights information including demographic, laboratory and procedure information as frequently used to enable predictive analytics. The company may wish to gather further information in the future to identify those strongest predictors for a number of different patient outcomes. In addition, the data science themes described earlier in the report (Factor Analysis and Markov Models) can also be incorporated into these analyses to reduce the number of input features and to stratify health and lifestyle factors.




# Images

![alt text](./images/potter.png)
*  Figure 1: Wireframe of the POTTER interface

![alt text](./images/MySurgeryRisk.png)
*  Figure 2: The conceptual diagram of MySurgeryRisk Intelligent Perioperative Platform

![alt text](./images/siccoli.png)
*  Figure 3: Feature importance based on AUC analysis


## References

* [1] Jun S. Kim, Varun Arvind, Eric K. Oermann, Deepak Kaji, Will Ranson, Chierika Ukogu, Awais K. Hussain, John Caridi, and Samuel K. Cho.  Predicting Surgical Complications in Patients Undergoing Elective Adult Spinal Deformity Procedures Using Machine Learning. Spine Deformity, 6(6):762–770, November 2018.
* [2] Brian L. Hill, Robert Brown, Eilon Gabel, Nadav Rakocz, Christine Lee, Maxime Cannesson, Pierre Baldi, Loes Olde Loohuis, Ruth Johnson, Brandon Jew, Uri Maoz, Aman Mahajan, Sriram Sankararaman, Ira Hofer, and Eran Halperin. An automated machine learning-based model predicts postoperative mortality using readily-extractable preoperative electronic health record data.
British Journal of Anaesthesia, page S0007091219306464, October 2019.
* [3] Alexander Meyer, Dina Zverinski, Boris Pfahringer, Jörg Kempfert, Titus Kuehne, Simon H Sündermann, Christof Stamm, Thomas Hofmann, Volkmar Falk, and Carsten Eickhoff.  Machine learning for real-time prediction of complications in critical care: a retrospective study. The Lancet Respiratory Medicine, 6(12):905–914, December 2018.
* [4] Kim A. Eagle and Rachel Eshima McKay. Pre-Operative Risk Prediction. Journal of the American College of Cardiology, 73(24):3079–3081, June 2019.
* [5] Summer S. Han, Tej D. Azad, Paola A. Suarez, and John K. Ratliff. A machine learning approach for predictive models of adverse events following spine surgery. The Spine Journal, 19(11):1772–1781, November 2019.
* [6] Dimitris Bertsimas, Jack Dunn, George C. Velmahos, and Haytham M. A. Kaafarani. Surgical Risk Is Not Linear: Derivation and Validation of a Novel, User-friendly, and Machine-learning-based Predictive OpTimal Trees in Emergency Surgery Risk (POTTER) Calculator. Annals of Surgery, 268(4):574–583, October 2018.
* [7] Azra Bihorac, Tezcan Ozrazgat-Baslanti, Ashkan Ebadi, Amir Motaei, Mohcine Madkour, Panagote M. Pardalos, Gloria Lipori, William R. Hogan, Philip A. Efron, Frederick Moore, Lyle L. Moldawer, Daisy Zhe Wang, Charles E. Hobson, Parisa Rashidi, Xiaolin Li, and Petar Momcilovic.  MySurgeryRisk:  Development and Validation of
a Machine-learning Risk Algorithm for Major Complications and Death After Surgery. Annals of Surgery, 269(4):652–662, April 2019.
* [8] Anish GR Potty, Ajish S. R. Potty, Rithesh Punyamurthula, Sreeram Penna, Chris Benavides, Prithviraj Chavan, and  R.  Justin  Mistovich.   MACHINE-LEARNING  IDENTIFIES  BEST  MEASURES  TO  PREDICT  ACL RECONSTRUCTION OUTCOME. Orthopaedic Journal of Sports Medicine, 7(3_suppl):2325967119S0014, March 2019.
* [9] Sandeep Reddy, John Fox, and Maulik P Purohit. Artificial intelligence-enabled healthcare delivery. Journal of the Royal Society of Medicine, 112(1):22–28, January 2019.
* [10] Manuel Huber, Christoph Kurz, and Reiner Leidl. Predicting patient-reported outcomes following hip and knee replacement surgery using supervised machine learning. BMC Medical Informatics and Decision Making, 19(1):3, December 2019.
* [11]  J. Madison Hyer, Susan White, Jordan Cloyd, Mary Dillhoff, Allan Tsung, Timothy M. Pawlik, and Aslam Ejaz. Can We Improve Prediction of Adverse Surgical Outcomes? Development of a Surgical Complexity Score Using a Novel Machine Learning Technique. Journal of the American College of Surgeons, October 2019.
* [12] Calvin J. Chiew, Nan Liu, Ting Hway Wong, Yilin E. Sim, and Hairil R. Abdullah. Utilizing Machine Learning Methods for Preoperative Prediction of Postsurgical Mortality and Intensive Care Unit Admission:. Annals of Surgery, page 1, April 2019.
* [13] Anita Brobbey, Peter Faris, Alberto Nettel-Aguirre, Tyler Williamson, Samuel Wiebe, Mingkai Peng, Hude Quan, and Tolulope T Sajobi. Ensemble-based Classification Models for Predicting Post-Operative Mortality Risk in Coronary Artery Disease. International Journal of Population Data Science, 3(4), September 2018.
* [14] Katiuscha Merath, J. Madison Hyer, Rittal Mehta, Ayesha Farooq, Fabio Bagante, Kota Sahara, Diamantis I. Tsilimigras, Eliza Beal, Anghela Z. Paredes, Lu Wu, Aslam Ejaz, and Timothy M. Pawlik.  Use of Machine Learning for Prediction of Patient Risk of Postoperative Complications After Liver, Pancreatic, and Colorectal
Surgery. Journal of Gastrointestinal Surgery, August 2019.
* [15] Christiaan H B van Niftrik, Frank van der Wouden, Victor E Staartjes, Jorn Fierstra, Martin N Stienen, Kevin Akeret, Martina Sebök, Tommaso Fedele, Johannes Sarnthein, Oliver Bozinov, Niklaus Krayenbühl, Luca Regli, and Carlo Serra.  Machine Learning Algorithm Identifies Patients at High Risk for Early Complications After Intracranial Tumor Surgery: Registry-Based Cohort Study.
Neurosurgery, 85(4):E756–E764, October 2019.
* [16] Clemence Petit, Rick Bezemer, and Louis Atallah. A review of recent advances in data analytics for post-operative patient deterioration detection. Journal of Clinical Monitoring and Computing, 32(3):391–402, June 2018.
* [17] Ahmed F Attaallah, Manuel C Vallejo, Osama M Elzamzamy, Michael G Mueller, and Warren S Eller. Perioperative risk factors for postoperative respiratory failure. Journal of perioperative practice, 29(3):49–53, 2019.
* [18] Marco Antonio Mascarella, Daniel Milad, Keith Richardson, Alex Mlynarek, Richard J. Payne, Veronique-Isabelle Forest, Michael Hier, Nader Sadeghi, and Nancy Mayo. Preoperative Risk Index Among Patients Undergoing Thyroid or Parathyroid Surgery. JAMA Otolaryngology–Head & Neck Surgery, 2019.
* [19] Harun Kundi, Rishi K. Wadhera, Jordan B. Strom, Linda R. Valsdottir, Changyu Shen, Dhruv S. Kazi, and Robert W. Yeh. Association of Frailty With 30-Day Outcomes for Acute Myocardial Infarction, Heart Failure, and Pneumonia Among Elderly Adults. JAMA Cardiology, 2019.
* [20] Blessing N R Jaja, Gustavo Saposnik, Hester F Lingsma, Erin Macdonald, Kevin E Thorpe, Muhammed Mamdani, Ewout W Steyerberg, Andrew Molyneux, Airton Leonardo de Oliveira Manoel, Bawarjan Schatlo, Daniel Hanggi, David Hasan, George K C Wong, Nima Etminan, Hitoshi Fukuda, James Torner, Karl L Schaller, Jose I Suarez, Martin N Stienen, Mervyn D I Vergouwen, Gabriel J E Rinkel, Julian Spears, Michael D Cusimano, Michael Todd, Peter Le Roux, Peter Kirkpatrick, John Pickard, Walter M van den Bergh, Gordon Murray, S Claiborne Johnston, Sen Yamagata, Stephan Mayer, Tom A Schweizer, and R Loch Macdonald. Development and validation of outcome prediction models for aneurysmal subarachnoid haemorrhage: the SAHIT multinational cohort study.
BMJ, 360, 2018.
* [21] Anne H Blaes and Chetan Shenoy. Is it time to include cancer in cardiovascular risk prediction tools? The Lancet, 394(10203):986–988, September 2019.
* [22] Chaoqun Wu, Xiqian Huo, Jiamin Liu, Lihua Zhang, Jiapeng Lu, Xueke Bai, Shuang Hu, Xi Li, Xin Zheng, Haibo Zhang, and Jing Li. Development and validation of a risk-prediction model for in-hospital major adverse cardiovascular events in patients admitted to hospital with acute myocardial infarction: results from China PEACE retrospective and prospective studies. The Lancet, 394:S17, October 2019.
* [23] Joaquim Michael Havens, Alexandra B Columbus, Anupamaa J Seshadri, Carlos V R Brown, Gail T Tominaga, Nathan T Mowery, and Marie Crandall. Risk stratification tools in emergency general surgery.
Trauma surgery & acute care open, 3(1):e000160–e000160, April 2018.
* [24] Nishant Sahni, Gyorgy Simon, and Rashi Arora. Development and Validation of Machine Learning Models for Prediction of 1-Year Mortality Utilizing Electronic Medical Record Data Available at the End of Hospitalization in Multicondition Patients: a Proof-of-Concept Study. Journal of general internal medicine, 33(6):921–928, June 2018.
* [25] Bowen Su, Roger Newson, Harry Soljak, and Michael Soljak. Associations between post-operative rehabilitation of hip fracture and outcomes:  national database analysis (90 characters).
BMC musculoskeletal disorders, 19(1):211–211, July 2018.
* [26] Anna Mende, Ann-Kathrin Riegel, Lili Plümer, Cynthia Olotu, Alwin E Goetz, and Rainer Kiefmann. Determinants of Perioperative Outcome in Frail Older Patients.
Deutsches Arzteblatt international, 116(5):73–82, February 2019.
* [27] Salomeh Keyhani,  Erin Madden,  Eric M Cheng,  Dawn M Bravata,  Ethan Halm,  Peter C Austin,  Mehrnaz Ghasemiesfe, Ann S Abraham, Alysandra J Zhang, and Jason M Johanning. Risk Prediction Tools to Improve Patient Selection for Carotid Endarterectomy Among Patients With Asymptomatic Carotid Stenosis. JAMA surgery, 154(4):336–344, April 2019.
* [28] Zhe Li and Xin Ding. The incremental predictive value of frailty measures in elderly patients undergoing cardiac surgery: A systematic review. Clinical cardiology, 41(8):1103–1110, August 2018.
* [29] Vanesa Bellou, Lazaros Belbasis, Athanasios K Konstantinidis, Ioanna Tzoulaki, and Evangelos Evangelou. Prognostic models for outcome prediction in patients with chronic obstructive pulmonary disease: systematic review and critical appraisal. BMJ, 367, 2019.
* [30] Alessandro Siccoli, Marlies P. de Wispelaere, Marc L. Schröder, and Victor E. Staartjes. Machine learning–based preoperative predictive analytics for lumbar spinal stenosis. Neurosurgical Focus, 46(5):E5, May 2019.

## Tools and Techniques

- *Logistic Regression* https://bit.ly/dia-logistic-regression
- Choosing the Right Type of *Regression* Analysis https://bit.ly/dia-regression-techniques
- *Random Forest* Models https://bit.ly/dia-random-forest
- *Gradient Boosting Machines* https://bit.ly/dia-gradient-boosting
- *Neural Networks* https://bit.ly/dia-neural-networks
