# List of publications

**(Items in bold indicate publications where I was the leading author)**

1. Search for the Xb and other hidden-beauty states in the π+π−Υ(1S) channel at ATLAS, ATLAS Collaboration, Phys.Lett. B740 (2015) 199-217
2. Search for s-channel single top-quark production in proton–proton collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B740 (2014) 118-136
3. Search for neutral Higgs bosons of the minimal supersymmetric standard model in pp collisions at s√ = 8 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1411 (2014) 056
4. Search for nonpointing and delayed photons in the diphoton and missing transverse momentum final state in 8 TeV pp collisions at the LHC using the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 11, 112005
5. Search for pair and single production of new heavy quarks that decay to a Z boson and a third-generation quark in pp collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1411 (2014) 104
6. Measurement of distributions sensitive to the underlying event in inclusive Z-boson production in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 12, 3195
7. Search for H→γγ produced in association with top quarks and constraints on the Yukawa coupling between the top quark and the Higgs boson using data taken at 7 TeV and 8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B740 (2014) 222-242
8. Measurement of long-range pseudorapidity correlations and azimuthal harmonics in √sNN=5.02 TeV proton-lead collisions with the ATLAS detector, ATLAS Collaboration, Phys.Rev. C90 (2014) 4, 044906
9. Search for long-lived neutral particles decaying into lepton jets in proton-proton collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1411 (2014) 088
10. Measurement of Higgs boson production in the diphoton decay channel in pp collisions at center-of-mass energies of 7 and 8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 112015
11. A measurement of the ratio of the production cross sections for W and Z bosons in association with jets with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 12, 3168
12. Measurement of the total cross section from elastic scattering in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Nucl.Phys. B889 (2014) 486-548
13. Search for the lepton flavor violating decay Z→eμ in pp collisions at s√  TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 7, 072010
14. Measurement of flow harmonics with multi-particle cumulants in Pb+Pb collisions at √sNN=2.76 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 11, 3157
15. Fiducial and differential cross sections of Higgs boson production measured in the four-lepton decay channel in pp collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B738 (2014) 234-253
16. Search for new resonances in Wγ and Zγ final states in pp collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B738 (2014) 428-447
17. Search for new particles in events with one lepton and missing transverse momentum in pp collisions at s√ = 8 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1409 (2014) 037
18. Search for Scalar Diphoton Resonances in the Mass Range 65−600 GeV with the ATLAS Detector in pp Collision Data at s√ = 8 TeV, ATLAS Collaboration, Phys.Rev.Lett. 113 (2014) 17, 171801
19. Measurements of jet vetoes and azimuthal decorrelations in dijet events produced in pp collisions at s√ = 7 TeV using the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 11, 3117
20. Measurement of the production cross-section of ψ(2S)→J/ψ(→μ+μ−)π+π− in pp collisions at s√ = 7 TeV at ATLAS, ATLAS Collaboration, JHEP 1409 (2014) 79
21. Electron and photon energy calibration with the ATLAS detector using LHC Run 1 data, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 10, 3071
22. Measurements of spin correlation in top-antitop quark events from proton-proton collisions at s√=7 TeV using the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 112016
23. Measurements of fiducial and differential cross sections for Higgs boson production in the diphoton decay channel at s√=8 TeV with ATLAS, ATLAS Collaboration, JHEP 1409 (2014) 112
24. Measurement of the muon reconstruction performance of the ATLAS detector using 2011 and 2012 LHC proton-proton collision data, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 11, 3130
25. Measurement of differential production cross-sections for a Z boson in association with b-jets in 7 TeV proton-proton collisions with the ATLAS detector, ATLAS Collaboration, JHEP 1410 (2014) 141
26. Search for contact interactions and large extra dimensions in the dilepton channel using proton-proton collisions at s√ = 8 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 12, 3134
27. Flavor tagged time-dependent angular analysis of the Bs→J/ψϕ decay and extraction of ΔΓs and the weak phase ϕs in ATLAS, ATLAS Collaboration, Phys.Rev. D90 (2014) 5, 052007
28. Observation of an Excited Bc+/- Meson State with the ATLAS Detector, ATLAS Collaboration, Phys.Rev.Lett. 113 (2014) 212004
29. Measurement of the tt¯ production cross-section as a function of jet multiplicity and jet transverse momentum in 7 TeV proton-proton collisions with the ATLAS detector, ATLAS Collaboration, JHEP01(2015)020
30. Measurement of the cross-section of high transverse momentum vector bosons reconstructed as single jets and studies of jet substructure in pp collisions at s√ = 7 TeV with the ATLAS detector, ATLAS Collaboration, New J.Phys. 16 (2014) 11, 113013
31. Search for pair-produced third-generation squarks decaying via charm quarks or in compressed supersymmetric scenarios in pp collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 052008
32. Search for supersymmetry in events with large missing transverse momentum, jets, and at least one tau lepton in 20 fb−1 of s√= 8 TeV proton-proton collision data with the ATLAS detector, ATLAS Collaboration, JHEP 1409 (2014) 103
33. Search for strong production of supersymmetric particles in final states with missing transverse momentum and at least three b-jets at s√= 8 TeV proton-proton collisions with the ATLAS detector, ATLAS Collaboration, JHEP 1410 (2014) 24
34. Search for top squark pair production in final states with one isolated lepton, jets, and missing transverse momentum in s√=8 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, JHEP 1411 (2014) 118
35. Measurements of normalized differential cross sections for tt¯ production in pp collisions at s√=7  TeV using the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 7, 072004
36. Search for the direct production of charginos, neutralinos and staus in final states with at least two hadronically decaying taus and missing transverse momentum in pp collisions at s√ = 8 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1410 (2014) 96
37. Comprehensive measurements of t-channel single top-quark production cross sections at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 112006
38. A neural network clustering algorithm for the ATLAS silicon pixel detector, ATLAS Collaboration, JINST 9 (2014) P09009
39. Search for the Standard Model Higgs boson decay to μ+μ− with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B738 (2014) 68-86
40. **Many-core on the Grid: From exploration to production, T. Doherty, M. Doidge, T. Ferrari, L. Salvador, J. Walsh, A. Washbrook. 2014. 5 pp., J.Phys.Conf.Ser. 513 (2014) 052037**
41. **Leveraging HPC resources for High Energy Physics, B. O'Brien (Edinburgh U.), R. Walker (Munich U.), A. Washbrook (Edinburgh U.). 2014. 5 pp., J.Phys.Conf.Ser. 513 (2014) 032104**
42. HEPDOOP: High-Energy Physics Analysis using Hadoop, W. Bhimji, T. Bristow, A. Washbrook (Edinburgh U.). 2014. 5 pp., J.Phys.Conf.Ser. 513 (2014) 022004
43. Measurement of the tt⎯ production cross-section using eμ events with b -tagged jets in pp collisions at s√=7 and 8 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 10, 3109
44. Search for WZ resonances in the fully leptonic channel using pp collisions at s√ = 8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B737 (2014) 223-243
45. Measurement of the Higgs boson mass from the H→γγ and H→ZZ∗→4ℓ channels with the ATLAS detector using 25 fb−1 of pp collision data, ATLAS Collaboration, Phys.Rev. D90 (2014) 052004
46. Measurement of the Z/γ∗ boson transverse momentum distribution in pp collisions at s√ = 7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1409 (2014) 145
47. Measurement of inclusive jet charged-particle fragmentation functions in Pb+Pb collisions at √sNN=2.76 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B739 (2014) 320-342
48. Search for direct pair production of the top squark in all-hadronic final states in proton-proton collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1409 (2014) 015
49. Measurement of the underlying event in jet events from 7 TeV proton-proton collisions with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 8, 2965
50. Search for squarks and gluinos with the ATLAS detector in final states with jets and missing transverse momentum using s√=8 TeV proton-proton collision data, ATLAS Collaboration, JHEP 1409 (2014) 176
51. Light-quark and gluon jet discrimination in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 8, 3023
52. Evidence for Electroweak Production of W±W±jj in pp Collisions at s√=8 TeV with the ATLAS Detector, ATLAS Collaboration, Phys.Rev.Lett. 113 (2014) 14, 141803
53. Search for supersymmetry in events with four or more leptons in s√ = 8 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 052001
54. Search for microscopic black holes and string balls in final states with leptons and jets with the ATLAS detector at sqrt(s) = 8 TeV, ATLAS Collaboration, JHEP 1408 (2014) 103
55. Search for high-mass dilepton resonances in pp collisions at s√=8  TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 5, 052005
56. Measurement of the centrality and pseudorapidity dependence of the integrated elliptic flow in lead-lead collisions at √sNN=2.76 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 8, 2982
57. Monitoring and data quality assessment of the ATLAS liquid argon calorimeter, ATLAS Collaboration, JINST 9 (2014) P07024
58. Measurement of the cross section of high transverse momentum Z→bb¯ production in proton-proton collisions at s√=8TeV with the ATLAS Detector, ATLAS Collaboration, Phys.Lett. B738 (2014) 25-43
59. Measurement of χc1 and χc2 production with s√ = 7 TeV pp collisions at ATLAS, ATLAS Collaboration, JHEP 1407 (2014) 154
60. Muon reconstruction efficiency and momentum resolution of the ATLAS experiment in proton-proton collisions at s√ = 7 TeV in 2010, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 9, 3034
61. Search for supersymmetry at s√=8 TeV in final states with jets and two same-sign leptons or three leptons with the ATLAS detector, ATLAS Collaboration, JHEP 1406 (2014) 035
62. Electron reconstruction and identification efficiency measurements with the ATLAS detector using the 2011 LHC proton-proton collision data ATLAS Collaboration, Eur.Phys.J. C74 (2014) 7, 2941
63. Measurement of the low-mass Drell-Yan differential cross section at s√ = 7 TeV using the ATLAS detector, ATLAS Collaboration, JHEP 1406 (2014) 112
64. Measurement of the parity-violating asymmetry parameter αb and the helicity amplitudes for the decay Λ0b→J/ψ+Λ0 with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D89 (2014) 9, 092009
65. Search for dark matter in events with a Z boson and missing transverse momentum in pp collisions at s√=8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D90 (2014) 1, 012004
66. Search for top quark decays t→qH with H→γγ using the ATLAS detector, ATLAS Collaboration, JHEP 1406 (2014) 008
67. Measurements of Four-Lepton Production at the Z Resonance in pp Collisions at s√=7 and 8 TeV with ATLAS, ATLAS Collaboration, Phys.Rev.Lett. 112 (2014) 23, 231806
68. Search for direct production of charginos, neutralinos and sleptons in final states with two leptons and missing transverse momentum in pp collisions at s√= 8 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1405 (2014) 071
69. Search for direct top squark pair production in events with a Z boson, b-jets and missing transverse momentum in sqrt(s)=8 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 6, 2883
70. Search for direct top-squark pair production in final states with two leptons in pp collisions at s√= 8TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1406 (2014) 124
71. Measurement of event-plane correlations in √sNN=2.76 TeV lead-lead collisions with the ATLAS detector, ATLAS Collaboration, Phys.Rev. C90 (2014) 2, 024905
72. Search for direct production of charginos and neutralinos in events with three leptons and missing transverse momentum in s√= 8TeV pp collisions with the ATLAS detector, ATLAS Collaboration, JHEP 1404 (2014) 169
73. Measurement of the production of a W boson in association with a charm quark in pp collisions at s√= 7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1405 (2014) 068
74. The differential production cross section of the ϕ (1020) meson in s√ = 7 TeV pp collisions measured with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C74 (2014) 7, 2895
75. Search for Invisible Decays of a Higgs Boson Produced in Association with a Z Boson in ATLAS, ATLAS Collaboration, Phys.Rev.Lett. 112 (2014) 201802
76. Search for Higgs boson decays to a photon and a Z boson in pp collisions at s√=7 and 8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B732 (2014) 8-27
77. Measurement of the electroweak production of dijets in association with a Z-boson and distributions sensitive to vector boson fusion in proton-proton collisions at s√= 8 TeV using the ATLAS detector, ATLAS Collaboration, JHEP 1404 (2014) 031
78. Measurement of the production cross section of prompt J/ψ mesons in association with a W± boson in pp collisions at s√= 7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1404 (2014) 172
79. Measurement of dijet cross sections in pp collisions at 7 TeV centre-of-mass energy using the ATLAS detector, ATLAS Collaboration, JHEP 1405 (2014) 059
80. Search for a multi-Higgs-boson cascade in W+W−bb¯ events with the ATLAS detector in pp collisions at s√=8  TeV, ATLAS Collaboration, Phys.Rev. D89 (2014) 3, 032002
81. Standalone vertex finding in the ATLAS muon spectrometer, ATLAS Collaboration, JINST 9 (2014) P02001
82. **The performance and development of the ATLAS Inner Detector Trigger, A.Washbrook, JINST 9 C02012**
83. Measurement of the top quark pair production charge asymmetry in proton-proton collisions at s√ = 7 TeV using the ATLAS detector, ATLAS Collaboration, JHEP 1402 (2014) 107
84. Search for Quantum Black Hole Production in High-Invariant-Mass Lepton+Jet Final States Using pp Collisions at s√= 8  TeV and the ATLAS Detector, ATLAS Collaboration, Phys.Rev.Lett. 112 (2014) 9, 091804
85. Measurement of the inclusive isolated prompt photons cross section in pp collisions at s√=7  TeV with the ATLAS detector using 4.6  fb−1, ATLAS Collaboration, Phys.Rev. D89 (2014) 5, 052004
86. Search for long-lived stopped R-hadrons decaying out-of-time with pp collisions using the ATLAS detector, ATLAS Collaboration, Phys.Rev. D88 (2013) 11, 112003
87. Measurement of the mass difference between top and anti-top quarks in pp collisions at (√s)=7 TeV using the ATLAS detector, ATLAS Collaboration, Phys.Lett. B728 (2014) 363-379
88. Search for charginos nearly mass degenerate with the lightest neutralino based on a disappearing-track signature in pp collisions at (√s)=8  TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D88 (2013) 11, 112006
89. Search for dark matter in events with a hadronically decaying W or Z boson and missing transverse momentum in pp collisions at s√= 8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 112 (2014) 4, 041802
90. Search for new phenomena in photon+jet events collected in proton-proton collisions at sqrt(s) = 8 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B728 (2014) 562-578
91. Search for microscopic black holes in a like-sign dimuon final state using large track multiplicity with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D88 (2013) 7, 072001
92. Search for direct third-generation squark pair production in final states with missing transverse momentum and two b-jets in s√= 8 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, JHEP 1310 (2013) 189
93. Search for new phenomena in final states with large jet multiplicities and missing transverse momentum at s√=8 TeV proton-proton collisions using the ATLAS experiment, ATLAS Collaboration, JHEP 1310 (2013) 130, Erratum-ibid. 1401 (2014) 109
94. Search for excited electrons and muons in s√=8 TeV proton-proton collisions with the ATLAS detector, ATLAS Collaboration, New J.Phys. 15 (2013) 093011
95. Dynamics of isolated-photon plus jet production in pp collisions at (√s)=7 TeV with the ATLAS detector, ATLAS Collaboration, Nucl.Phys. B875 (2013) 483-535
96. Measurement of Top Quark Polarization in Top-Antitop Events from Proton-Proton Collisions at s√ = 7  TeV Using the ATLAS Detector, ATLAS Collaboration, Phys.Rev.Lett. 111 (2013) 23, 232002
97. Measurement of jet shapes in top-quark pair events at s√ = 7 TeV using the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 12, 2676
98. Measurement of the top quark charge in pp collisions at s√= 7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1311 (2013) 031
99. Evidence for the spin-0 nature of the Higgs boson using ATLAS data, ATLAS Collaboration, Phys.Lett. B726 (2013) 120-144
100. Measurements of Higgs boson production and couplings in diboson final states with the ATLAS detector at the LHC, ATLAS Collaboration, Phys.Lett. B726 (2013) 88-119, Erratum-ibid. B734 (2014) 406-406
101. Measurement of the differential cross-section of B+ meson production in pp collisions at s√ = 7 TeV at ATLAS, ATLAS Collaboration, JHEP 1310 (2013) 042
102. Measurement of the Azimuthal Angle Dependence of Inclusive Jet Yields in Pb+Pb Collisions at √sNN= 2.76 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 111 (2013) 15, 152301
103. Performance of jet substructure techniques for large-R jets in proton-proton collisions at s√ = 7 TeV using the ATLAS detector, ATLAS Collaboration, JHEP 1309 (2013) 076
104. Measurement of the high-mass Drell
105. an differential cross-section in pp collisions at sqrt(s)=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B725 (2013) 223-242
106. Measurement of the distributions of event-by-event flow harmonics in lead-lead collisions at = 2.76 TeV with the ATLAS detector at the LHC, ATLAS Collaboration, JHEP 1311 (2013) 183
107. Search for tt¯ resonances in the lepton plus jets final state with ATLAS using 4.7 fb−1 of pp collisions at s√=7 TeV, ATLAS Collaboration, Phys.Rev. D88 (2013) 1, 012004
108. Triggers for displaced decays of long-lived neutral particles in the ATLAS detector, ATLAS Collaboration, JINST 8 (2013) P07015
109. Search for resonant diboson production in the WW/WZ→ℓνjj decay channels with the ATLAS detector at s√ = 7  TeV, ATLAS Collaboration, Phys.Rev. D87 (2013) 11, 112006
110. Measurement of the production cross section of jets in association with a Z boson in pp collisions at s√ = 7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1307 (2013) 032
111. Study of heavy-flavor quarks produced in association with top-quark pairs at s√=7  TeV using the ATLAS detector, ATLAS Collaboration, Phys.Rev. D89 (2014) 7, 072012
112. Search for nonpointing photons in the diphoton and EmissT final state in s√=7  TeV proton-proton collisions using the ATLAS detector, ATLAS Collaboration, Phys.Rev. D88 (2013) 1, 012001
113. Measurement of the inclusive jet cross section in pp collisions at sqrt(s)=2.76 TeV and comparison to the inclusive jet cross section at sqrt(s)=7 TeV using the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 8, 2509
114. A particle consistent with the Higgs Boson observed with the ATLAS Detector at the Large Hadron Collider, ATLAS Collaboration, Science 338 (2012) 1576-1582
115. Measurement with the ATLAS detector of multi-particle azimuthal correlations in p+Pb collisions at sqrt(s_NN)=5.02 TeV, ATLAS Collaboration, Phys.Lett. B725 (2013) 60-78
116. Search for third generation scalar leptoquarks in pp collisions at s√ = 7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1306 (2013) 033
117. Characterisation and mitigation of beam-induced backgrounds observed in the ATLAS detector during the 2011 proton-proton run, ATLAS Collaboration, JINST 8 (2013) P07004
118. Search for WH production with a light Higgs boson decaying to prompt electron-jets in proton-proton collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, New J.Phys. 15 (2013) 043009
119. Improved luminosity determination in pp collisions at s√ = 7 TeV using the ATLAS detector at the LHC, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 8, 2518
120. Search for a light charged Higgs boson in the decay channel H+→cs¯ in tt¯ events using pp collisions at s√ = 7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 6, 2465
121. Electroweak Measurements in Electron-Positron Collisions at W-Boson-Pair Energies at LEP, ALEPH and DELPHI and L3 and OPAL and LEP Electroweak Collaboration,s (S. Schael (Aachen, Tech. Hochsch.) et al.). Feb 14, 2013. 126 pp., Phys.Rept. 532 (2013) 119-244
122. Measurement of the cross-section for W boson production in association with b-jets in pp collisions at s√ = 7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1306 (2013) 084
123. Measurement of kT splitting scales in W->lv events at sqrt(s)=7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 5, 2432
124. Measurements of Wγ and Zγ production in pp collisions at s√=7  TeV with the ATLAS detector at the LHC, ATLAS Collaboration, Phys.Rev. D87 (2013) 11, 112003
125. Measurement of hard double-parton interactions in W(→lν)+ 2 jet events at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, New J.Phys. 15 (2013) 033038
126. Search for Charged Higgs bosons: Combined Results Using LEP Data, ALEPH and DELPHI and L3 and OPAL and LEP Collaboration,s (G. Abbiendi (INFN, Bologna & Bologna U.) et al.). Jan 2013. 18 pp., Eur.Phys.J. C73 (2013) 2463
127. Search for long-lived, multi-charged particles in pp collisions at s√=7 TeV using the ATLAS detector, ATLAS Collaboration, Phys.Lett. B722 (2013) 305-323
128. **Multi-core job submission and grid resource scheduling for ATLAS AthenaMP, D. Crooks (Glasgow U.), P. Calafiura (LBL, Berkeley), R. Harrington (Edinburgh U.), M. Jha (INFN, Bologna & INFN, CNAF), T. Maeno (Brookhaven), S. Purdie (Edinburgh U.), H. Severini (Oklahoma U.), S. Skipsey (Glasgow U.), V. Tsulaia (LBL, Berkeley), R. Walker (Munich U.) et al.. 2012. 11 pp., J.Phys.Conf.Ser. 396 (2012) 032115**
129. **Acceleration of multivariate analysis techniques in TMVA using GPUs, A. Hoecker (CERN), H. McKendrick (Edinburgh U.), J. Therhaag (Bonn U.), A. Washbrook (Edinburgh U.). 2012. 10 pp., J.Phys.Conf.Ser. 396 (2012) 022055**
130. Search for single b∗-quark production with the ATLAS detector at s√=7 TeV, ATLAS Collaboration, Phys.Lett. B721 (2013) 171-189
131. Multi-channel search for squarks and gluinos in s√=7 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 3, 2362
132. A search for prompt lepton-jets in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B719 (2013) 299-317
133. Observation of Associated Near-Side and Away-Side Long-Range Correlations in √sNN=5.02  TeV Proton-Lead Collisions with the ATLAS Detector, ATLAS Collaboration, Phys.Rev.Lett. 110 (2013) 18, 182302
134. Search for charged Higgs bosons through the violation of lepton universality in tt¯ events using pp collision data at s√=7 TeV with the ATLAS experiment, ATLAS Collaboration, JHEP 1303 (2013) 076
135. Search for a heavy narrow resonance decaying to eμ, eτ, or μτ with the ATLAS detector in s√=7 TeV pp collisions at the LHC, ATLAS Collaboration, Phys.Lett. B723 (2013) 15-32
136. Measurement of Upsilon production in 7 TeV pp collisions at ATLAS, ATLAS Collaboration, Phys.Rev. D87 (2013) 5, 052004
137. Measurement of the ttbar production cross section in the tau+jets channel using the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 3, 2328
138. Search for the neutral Higgs bosons of the Minimal Supersymmetric Standard Model in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1302 (2013) 095
139. Measurement of angular correlations in Drell-Yan lepton pairs to probe Z/gamma* boson transverse momentum at sqrt(s)=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B720 (2013) 32-51
140. Search for new phenomena in events with three charged leptons at /sqrts = 7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D87 (2013) 5, 052002
141. Measurement of ZZ production in pp collisions at s√=7 TeV and limits on anomalous ZZZ and ZZγ couplings with the ATLAS detector, ATLAS Collaboration, JHEP 1303 (2013) 128
142. Search for resonances decaying into top-quark pairs using fully hadronic decays in pp collisions with ATLAS at s√=7 TeV, ATLAS Collaboration, JHEP 1301 (2013) 116
143. Measurement of isolated-photon pair production in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1301 (2013) 086
144. Searches for heavy long-lived sleptons and R-Hadrons with the ATLAS detector in pp collisions at s√=7 TeV, ATLAS Collaboration, Phys.Lett. B720 (2013) 277-308
145. Search for supersymmetry in events with photons, bottom quarks, and missing transverse momentum in proton-proton collisions at a centre-of-mass energy of 7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B719 (2013) 261-279
146. Search for contact interactions and large extra dimensions in dilepton events from pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D87 (2013) 015010
147. Search for Extra Dimensions in diphoton events using proton-proton collisions recorded at s√=7 TeV with the ATLAS detector at the LHC, ATLAS Collaboration, New J.Phys. 15 (2013) 043007
148. **Multicore in production: Advantages and limits of the multiprocess approach in the ATLAS experiment, S. Binet (Orsay, LAL), P. Calafiura (LBL, Berkeley), M.K. Jha (INFN, Bologna & INFN, CNAF), W. Lavrijsen, C. Leggett (LBL, Berkeley), D. Lesny (Illinois U., Urbana), H. Severini (Oklahoma U.), D. Smith (SLAC), S. Snyder (Brookhaven), M. Tatarkhanov (LBL, Berkeley) et al.. 2012., J.Phys.Conf.Ser. 368 (2012) 012018**
149. Search for long-lived, heavy particles in final states with a muon and multi-track displaced vertex in proton-proton collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B719 (2013) 280-298
150. A search for high-mass resonances decaying to τ+τ− in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B719 (2013) 242-260
151. Measurement of Z boson Production in Pb+Pb Collisions at √sNN=2.76 TeV with the ATLAS Detector, ATLAS Collaboration, Phys.Rev.Lett. 110 (2013) 022301
152. Jet energy resolution in proton-proton collisions at s√=7 TeV recorded in 2010 with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 3, 2306
153. Search for pair production of heavy top-like quarks decaying to a high-pT W boson and a b quark in the lepton plus jets final state at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B718 (2013) 1284-1302
154. Search for doubly-charged Higgs bosons in like-sign dilepton final states at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2244
155. Search for pair-produced massive coloured scalars in four-jet final states with the ATLAS detector in proton-proton collisions at s√=7 TeV, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 1, 2263
156. Search for pair production of massive particles decaying into three quarks with the ATLAS detector in s√=7 TeV pp collisions at the LHC, ATLAS Collaboration, JHEP 1212 (2012) 086
157. Search for anomalous production of prompt like-sign lepton pairs at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1212 (2012) 007
158. Search for dark matter candidates and large extra dimensions in events with a jet and missing transverse momentum with the ATLAS detector, ATLAS Collaboration, JHEP 1304 (2013) 075
159. Search for R-parity-violating supersymmetry in events with four or more leptons in s√=7 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, JHEP 1212 (2012) 124
160. Measurement of W+W− production in pp collisions at s√=7  TeV with the ATLAS detector and limits on anomalous WWZ and WWγ couplings, ATLAS Collaboration, Phys.Rev. D87 (2013) 11, 112001, Erratum-ibid. D88 (2013) 7, 079906
161. Search for direct chargino production in anomaly-mediated supersymmetry breaking models based on a disappearing-track signature in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1301 (2013) 131
162. ATLAS search for new phenomena in dijet mass and angular distributions using pp collisions at s√=7 TeV, ATLAS Collaboration, JHEP 1301 (2013) 029
163. Search for Supersymmetry in Events with Large Missing Transverse Momentum, Jets, and at Least One Tau Lepton in 7 TeV Proton-Proton Collision Data with the ATLAS Detector, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2215
164. Measurement of the flavour composition of dijet events in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 2, 2301
165. Search for displaced muonic lepton jets from light Higgs boson decay in proton-proton collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B721 (2013) 32-50
166. Search for resonant top plus jet production in tt¯ + jets events with the ATLAS detector in pp collisions at s√=7 TeV, ATLAS Collaboration, Phys.Rev. D86 (2012) 091103
167. Search for dark matter candidates and large extra dimensions in events with a photon and missing transverse momentum in pp collision data at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 110 (2013) 011802
168. ATLAS search for a heavy gauge boson decaying to a charged lepton and a neutrino in pp collisions at s√=7 TeV, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2241
169. Search for a heavy top-quark partner in final states with two leptons with the ATLAS detector at the LHC, ATLAS Collaboration, JHEP 1211 (2012) 094
170. Search for high-mass resonances decaying to dilepton final states in pp collisions at s**(1/2) = 7-TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1211 (2012) 138
171. Search for light top squark pair production in final states with leptons and b− jets with the ATLAS detector in s√=7 TeV proton-proton collisions, ATLAS Collaboration, Phys.Lett. B720 (2013) 13-31
172. Search for diphoton events with large missing transverse momentum in 7 TeV proton-proton collision data with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B718 (2012) 411-430
173. Measurements of the pseudorapidity dependence of the total transverse energy in proton-proton collisions at s√=7 TeV with ATLAS, ATLAS Collaboration, JHEP 1211 (2012) 033
174. Further search for supersymmetry at s√=7 TeV in final states with jets, missing transverse momentum and isolated leptons with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D86 (2012) 092002
175. Search for light scalar top quark pair production in final states with two leptons with the ATLAS detector in s√=7 TeV proton-proton collisions, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2237
176. Search for direct production of charginos and neutralinos in events with three leptons and missing transverse momentum in s√=7 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B718 (2013) 841-859
177. Search for direct slepton and gaugino production in final states with two leptons and missing transverse momentum with the ATLAS detector in pp collisions at s√=7 TeV, ATLAS Collaboration, Phys.Lett. B718 (2013) 879-901
178. Search for new phenomena in the WW to ℓνℓ' ν' final state in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B718 (2013) 860-878
179. Search for direct top squark pair production in final states with one isolated lepton, jets, and missing transverse momentum in s√=7 TeV pp collisions using 4.7 fb−1 of ATLAS data, ATLAS Collaboration, Phys.Rev.Lett. 109 (2012) 211803
180. Measurement of the jet radius and transverse momentum dependence of inclusive jet suppression in lead-lead collisions at √sNN= 2.76 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B719 (2013) 220-241
181. Search for a supersymmetric partner to the top quark in final states with jets and missing transverse momentum at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 109 (2012) 211802
182. Measurement of WZ production in proton-proton collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2173
183. Search for squarks and gluinos with the ATLAS detector in final states with jets and missing transverse momentum using 4.7 fb−1 of s√=7 TeV proton-proton collision data, ATLAS Collaboration, Phys.Rev. D87 (2013) 012008
184. Time-dependent angular analysis of the decay B0s→J/ψϕ and extraction of ΔΓs and the CP-violating weak phase ϕs by ATLAS, ATLAS Collaboration, JHEP 1212 (2012) 072
185. Underlying event characteristics and their dependence on jet size of charged-particle jet events in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D86 (2012) 072004
186. Observation of a new particle in the search for the Standard Model Higgs boson with the ATLAS detector at the LHC, ATLAS Collaboration, Phys.Lett. B716 (2012) 1-29
187. Measurement of charged-particle event shape variables in s√=7 TeV proton-proton interactions with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D88 (2013) 3, 032004
188. Search for magnetic monopoles in s√=7 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 109 (2012) 261803
189. Measurements of top quark pair relative differential cross-sections with ATLAS in pp collisions at s√=7 TeV, ATLAS Collaboration, Eur.Phys.J. C73 (2013) 1, 2261
190. Search for top and bottom squarks from gluino pair production in final states with missing transverse energy and at least three b-jets with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2174
191. A search for tt¯ resonances in lepton+jets events with highly boosted top quarks collected in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1209 (2012) 041
192. Measurement of the Λb lifetime and mass in the ATLAS experiment, ATLAS Collaboration, Phys.Rev. D87 (2013) 3, 032002
193. Combined search for the Standard Model Higgs boson in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D86 (2012) 032003
194. Search for the Standard Model Higgs boson produced in association with a vector boson and decaying to a b-quark pair with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B718 (2012) 369-390
195. Search for the Higgs boson in the H→WW→ lnujj decay channel at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B718 (2012) 391-410
196. Search for the Standard Model Higgs boson in the H to τ+τ− decay mode in s√=7 TeV pp collisions with ATLAS, ATLAS Collaboration, JHEP 1209 (2012) 070
197. ATLAS measurements of the properties of jets for boosted particle searches, ATLAS Collaboration, Phys.Rev. D86 (2012) 072006
198. Measurement of the b-hadron production cross section using decays to D∗μ−X final states in pp collisions at sqrt(s) = 7 TeV with the ATLAS detector, ATLAS Collaboration, Nucl.Phys. B864 (2012) 341-381
199. Search for a standard model Higgs boson in the mass range 200 - 600-GeV in the H→ZZ→ℓ+ℓ−qq¯ decay channel with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B717 (2012) 70-88
200. Measurement of event shapes at large momentum transfer with the ATLAS detector in pp collisions at s√=7 TeV, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2211
201. Hunt for new phenomena using large jet multiplicities and missing transverse momentum with ATLAS in 4.7 fb−1 of s√=7 TeV proton-proton collisions, ATLAS Collaboration, JHEP 1207 (2012) 167
202. Search for the Standard Model Higgs boson in the H→ WW(*) →ℓνℓν decay mode with 4.7 /fb of ATLAS data at s√=7 TeV, ATLAS Collaboration, Phys.Lett. B716 (2012) 62-81
203. **Algorithm acceleration from GPGPUs for the ATLAS upgrade, ATLAS Collaboration, J.Phys.Conf.Ser. 331 (2011) 022031**
204. A search for flavour changing neutral currents in top-quark decays in pp collision data collected with the ATLAS detector at s√=7 TeV, ATLAS Collaboration, JHEP 1209 (2012) 139
205. Search for a Standard Model Higgs boson in the H -> ZZ -> l+l−νν¯ decay channel using 4.7 fb−1 of sqrt(s) = 7 TeV data with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B717 (2012) 29-48
206. Evidence for the associated production of a W boson and a top quark in ATLAS at s√=7 TeV, ATLAS Collaboration, Phys.Lett. B716 (2012) 142-159
207. A search for tt¯ resonances with the ATLAS detector in 2.05 fb−1 of proton-proton collisions at s√=7 TeV, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2083
208. Measurement of Wγ and Zγ production cross sections in pp collisions at s√=7 TeV and limits on anomalous triple gauge couplings with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B717 (2012) 49-69
209. Measurement of the W boson polarization in top quark decays with the ATLAS detector, ATLAS Collaboration, JHEP 1206 (2012) 088
210. Measurement of the top quark pair cross section with ATLAS in pp collisions at sqrt(s) = 7 TeV using final states with an electron or a muon and a hadronically decaying τ lepton, ATLAS Collaboration, Phys.Lett. B717 (2012) 89-108
211. Search for tb resonances in proton-proton collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 109 (2012) 081801
212. Search for a fermiophobic Higgs boson in the diphoton decay channel with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2157
213. Search for scalar top quark pair production in natural gauge mediated supersymmetry models with the ATLAS detector in pp collisions at s√=7 TeV, ATLAS Collaboration, Phys.Lett. B715 (2012) 44-60
214. Measurement of τ polarization in W−>τν decays with the ATLAS detector in pp collisions at sqrt(s) = 7 TeV, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2062
215. Search for supersymmetry in events with three leptons and missing transverse momentum in s√=7 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 108 (2012) 261804
216. Hiding the complexity: Building a distributed ATLAS Tier-2 with a single resource interface using ARC middleware, S. Purdie, G. Stewart, M. Kenyon, S. Skipsey (Glasgow U.), A. Washbrook, W. Bhimji (Edinburgh U.), A. Filipcic (Stefan Inst., Ljubljana). 2011. 5 pp., J.Phys.Conf.Ser. 331 (2011) 072029
217. Search for supersymmetry with jets, missing transverse momentum and at least one hadronically decaying τ lepton in proton-proton collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B714 (2012) 197-214
218. Search for charged Higgs bosons decaying via H+→τν in top quark pair events using pp collision data at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, JHEP 1206 (2012) 039
219. Search for resonant WZ production in the WZ→ℓνℓ′ℓ′ channel in s√=7 TeV pp collisions with the ATLAS detector, ATLAS Collaboration, Phys.Rev. D85 (2012) 112012
220. Search for pair production of a new quark that decays to a Z boson and a bottom quark with the ATLAS detector, ATLAS Collaboration, Phys.Rev.Lett. 109 (2012) 071801
221. Search for the decay Bs0 -> mu mu with the ATLAS detector, ATLAS Collaboration, Phys.Lett. B713 (2012) 387-407
222. Measurement of the WW cross section in s√=7 TeV pp collisions with the ATLAS detector and limits on anomalous gauge couplings, ATLAS Collaboration, Phys.Lett. B712 (2012) 289-308
223. Measurement of tt¯ production with a veto on additional central jet activity in pp collisions at sqrt(s) = 7 TeV using the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2043
224. Search for second generation scalar leptoquarks in pp collisions at s√=7 TeV with the ATLAS detector, ATLAS Collaboration, Eur.Phys.J. C72 (2012) 2151
225. A study of the b-quark fragmentation function with the DELPHI detector at LEP I and an averaged distribution obtained at the Z Pole, DELPHI Collaboration, Eur.Phys.J. C71 (2011) 1557
226. Search for single top quark production via contact interactions at LEP2, DELPHI Collaboration, Eur.Phys.J. C71 (2011) 1555
227. Study of the Dependence of Direct Soft Photon Production on the Jet Characteristics in Hadronic Z^0 Decays, DELPHI Collaboration, Eur.Phys.J. C67 (2010) 343-366
228. Measurements of CP-conserving Trilinear Gauge Boson Couplings WWV (V = gamma,Z) in e+e- Collisions at LEP2, DELPHI Collaboration, Eur.Phys.J. C66 (2010) 35-56
229. Correlations between Polarisation States of W Particles in the Reaction e- e+ W- W+ at LEP2 Energies 189-GeV - 209-GeV, DELPHI Collaboration, Eur.Phys.J. C63 (2009) 611-623
230. Inclusive single-particle production in two-photon collisions at LEP II with the DELPHI detector, DELPHI Collaboration, Phys.Lett. B678 (2009) 444-449
231. Search for one large extra dimension with the DELPHI detector at LEP, DELPHI Collaboration, Eur.Phys.J. C60 (2009) 17-23
232. A Study of b anti-b Production in e+e- Collisions at s**(1/2) = 130-GeV - 207-GeV, DELPHI Collaboration, Eur.Phys.J. C60 (2009) 1-15
233. Di-jet production in gamma gamma collisions at LEP2, DELPHI Collaboration, Eur.Phys.J. C58 (2008) 531-541
234. Study of b-quark mass effects in multijet topologies with the DELPHI detector at LEP, DELPHI Collaboration, Eur.Phys.J. C55 (2008) 525-538
235. Measurement of the Mass and Width of the W Boson in e+e− Collisions at s√ = 161-GeV - 209-GeV, DELPHI Collaboration, Eur.Phys.J. C55 (2008) 1-38
236. Study of W boson polarisations and Triple Gauge boson Couplings in the reaction e+e- W+W- at LEP 2, DELPHI Collaboration, Eur.Phys.J. C54 (2008) 345-364
237. Observation of the Muon Inner Bremsstrahlung at LEP1, DELPHI Collaboration, Eur.Phys.J. C57 (2008) 499-514
238. Higgs boson searches in CP-conserving and CP-violating MSSM scenarios with the DELPHI detector, DELPHI Collaboration, Eur.Phys.J. C54 (2008) 1-35, Erratum-ibid. C56 (2008) 165-170
239. Measurement of the Tau Lepton Polarisation at LEP2, DELPHI Collaboration, Phys.Lett. B659 (2008) 65-73
240. Search for Pentaquarks in the Hadronic Decays of the Z Boson with the DELPHI Detector at LEP, DELPHI Collaboration, Phys.Lett. B653 (2007) 151-160
241. Study of triple-gauge-boson couplings ZZZ, ZZgamma and Zgamma gamma LEP, DELPHI Collaboration, Eur.Phys.J. C51 (2007) 525-542
242. Z gamma* production in e+e- interactions at s**(1/2) = 183 - 209-GeV, DELPHI Collaboration, Eur.Phys.J. C51 (2007) 503-523
243. Study of multi-muon bundles in cosmic ray showers detected with the DELPHI detector at LEP, DELPHI Collaboration, Astropart.Phys. 28 (2007) 273-286
244. Investigation of colour reconnection in WW events with the DELPHI detector at LEP-2, DELPHI Collaboration, Eur.Phys.J. C51 (2007) 249-269
245. Search for a fourth generation b′-quark at LEP-II at s√ = 196-GeV - 209-GeV, DELPHI Collaboration, Eur.Phys.J. C50 (2007) 507-518
246. Study of Leading Hadrons in Gluon and Quark Fragmentation, DELPHI Collaboration, Phys.Lett. B643 (2006) 147-157
247. Masses, Lifetimes and Production Rates of Xi- and anti-Xi+ at LEP 1, DELPHI Collaboration, Phys.Lett. B639 (2006) 179-191
248. Search for neutral MSSM Higgs bosons at LEP, ALEPH and DELPHI and L3 and OPAL and LEP Working Group for Higgs Boson Searches Collaboration,s (S. Schael (Aachen, Tech. Hochsch.) et al.). Jan 2006. 82 pp., Eur.Phys.J. C47 (2006) 547-587
249. A Determination of the centre-of-mass energy at LEP2 using radiative 2-fermion events, DELPHI Collaboration, Eur.Phys.J. C46 (2006) 295-305
250. Single intermediate vector boson production in e+e- collisions at s**(1/2) = 183-GeV to 209-GeV, DELPHI Collaboration, Eur.Phys.J. C45 (2006) 273-289
251. Evidence for an excess of soft photons in hadronic decays of Z0, DELPHI Collaboration, Eur.Phys.J. C47 (2006) 273-294
252. Determination of the b quark mass at the M(Z) scale with the DELPHI detector at LEP, DELPHI Collaboration, Eur.Phys.J. C46 (2006) 569-583
253. Study of double-tagged gamma gamma events at LEP II, DELPHI Collaboration, Eur.Phys.J. C46 (2006) 559-568
254. Measurement and interpretation of fermion-pair production at LEP energies above the Z resonance, DELPHI Collaboration, Eur.Phys.J. C45 (2006) 589-632
255. Charged particle multiplicity in three-jet events and two-gluon systems, DELPHI Collaboration, Eur.Phys.J. C44 (2005) 311-331
256. Determination of heavy quark non-perturbative parameters from spectral moments in semileptonic B decays, DELPHI Collaboration, Eur.Phys.J. C45 (2006) 35-59
257. Production of Xi0(c) and Xi(b) in Z decays and lifetime measurement of X(b), DELPHI Collaboration, Eur.Phys.J. C44 (2005) 299-309
258. Precision electroweak measurements on the Z resonance, ALEPH and DELPHI and L3 and OPAL and SLD and LEP Electroweak Working Group and SLD Electroweak Group and SLD Heavy Flavour Group Collaboration,s (S. Schael et al.). Sep 2005. 302 pp., Phys.Rept. 427 (2006) 257-454
259. Autonomic Management of Large Clusters and Their Integration into the Grid, Thomas Roblitz, Florian Schintke, Alexander Reinefeld (Konrad Zuse Zent. Informationstech.), Olof Barring, Maite Barroso Lopez, German Cancio, Sylvain Chapeland, Karim Chouikh, Lionel Cons, Piotr Poznanski (CERN) et al.. Sep 2004. 14 pp., J.Grid Comput. 2 (2004) 247-260
260. Flavour independent searches for hadronically decaying neutral Higgs bosons, DELPHI Collaboration, Eur.Phys.J. C44 (2005) 147-159
261. Search for excited leptons in e+ e- collisions at s**(1/2) = 189-GeV to 209-GeV, DELPHI Collaboration, Eur.Phys.J. C46 (2006) 277-293
262. Bose-Einstein correlations in W+ W- events at LEP2, DELPHI Collaboration, Eur.Phys.J. C44 (2005) 161-174
263. Determination of A**b(FB) at the Z pole using inclusive charge reconstruction and lifetime tagging, DELPHI Collaboration, Eur.Phys.J. C40 (2005) 1-25
264. Coherent soft particle production in Z decays into three jets, DELPHI Collaboration, Phys.Lett. B605 (2005) 37-48
265. Measurement of the energy dependence of hadronic jet rates and the strong coupling alpha(s) from the four-jet rate with the DELPHI detector at LEP, DELPHI Collaboration, Eur.Phys.J. C38 (2005) 413-426
266. Searches for neutral higgs bosons in extended models, DELPHI Collaboration, Eur.Phys.J. C38 (2004) 1-28
267. Determination of the e+ e- gamma gamma (gamma) cross-section at LEP 2, DELPHI Collaboration, Eur.Phys.J. C37 (2004) 405-419
268. The Measurement of alpha(s) from event shapes with the DELPHI detector at the highest LEP energies, DELPHI Collaboration, Eur.Phys.J. C37 (2004) 1-23
269. Measurement of |V(cb)| using the semileptonic decay anti-B0(d) D*+ l- anti-nu(l), DELPHI Collaboration, Eur.Phys.J. C33 (2004) 213-232
270. A Measurement of the tau hadronic branching ratios, DELPHI Collaboration, Eur.Phys.J. C46 (2006) 1-26
271. A Precise measurement of the tau lifetime, DELPHI Collaboration, Eur.Phys.J. C36 (2004) 283-296
272. Photon events with missing energy in e+ e- collisions at s**(1/2) = 130-GeV to 209-GeV, DELPHI Collaboration, Eur.Phys.J. C38 (2005) 395-411
273. Search for fermiophobic Higgs bosons in final states with photons at LEP 2, DELPHI Collaboration, Eur.Phys.J. C35 (2004) 313-324
274. Study of tau-pair production in photon-photon collisions at LEP and limits on the anomalous electromagnetic moments of the tau lepton, DELPHI Collaboration, Eur.Phys.J. C35 (2004) 159-170
275. Search for supersymmetric particles assuming R-parity nonconservation in e+ e- collisions at s**(1/2) = 192-GeV to 208-GeV, DELPHI Collaboration, Eur.Phys.J. C36 (2004) 1-23, Erratum-ibid. C37 (2004) 129-131
276. Search for single top production via FCNC at LEP at s√ = 189-GeV to 208-GeV, DELPHI Collaboration, Phys.Lett. B590 (2004) 21-34
277. Search for B0(s) - anti-B0(s) oscillations in DELPHI using high-p(t) leptons, DELPHI Collaboration, Eur.Phys.J. C35 (2004) 35-52
278. Search for charged Higgs bosons at LEP in general two Higgs doublet models, DELPHI Collaboration, Eur.Phys.J. C34 (2004) 399-418
279. Search for SUSY in the AMSB scenario with the DELPHI detector, DELPHI Collaboration, Eur.Phys.J. C34 (2004) 145-156
280. Measurement of the forward backward asymmetries of e+ e- Z b anti-b and e+ e- Z -> c anti-c using prompt leptons, DELPHI Collaboration, Eur.Phys.J. C34 (2004) 109-125
281. Measurement of the Lambda0(b) decay form-factor, DELPHI Collaboration, Phys.Lett. B585 (2004) 63-84
282. A Precise measurement of the B+, B0 and mean b hadron lifetime with the DELPHI detector at LEP1, DELPHI Collaboration, Eur.Phys.J. C33 (2004) 307-324
283. Searches for invisibly decaying Higgs bosons with the DELPHI detector at LEP, DELPHI Collaboration, Eur.Phys.J. C32 (2004) 475-492
284. Measurement of the W pair production cross-section and W branching ratios in e+ e- collisions at s**(1/2) = 161-GeV to 209-GeV, DELPHI Collaboration, Eur.Phys.J. C34 (2004) 127-144
285. Searches for supersymmetric particles in e+ e- collisions up to 208-GeV and interpretation of the results within the MSSM, DELPHI Collaboration, Eur.Phys.J. C31 (2003) 421-479
286. A Measurement of the branching fractions of the b quark into charged and neutral b hadrons, DELPHI Collaboration, Phys.Lett. B576 (2003) 29-42
287. Measurement of the e+ e- W+ W- gamma cross-section and limits on anomalous quartic gauge couplings with DELPHI, DELPHI Collaboration, Eur.Phys.J. C31 (2003) 139-147
288. The Eta(c)(2980) formation in two photon collisions at LEP energies, DELPHI Collaboration, Eur.Phys.J. C31 (2003) 481-489
289. Measurement of inclusive f(1)(1285) and f(1)(1420) production in Z decays with the DELPHI detector, DELPHI Collaboration, Phys.Lett. B569 (2003) 129-139
290. ZZ production in e+ e- interactions at s**(1/2) = 183-GeV to 209-GeV, DELPHI Collaboration, Eur.Phys.J. C30 (2003) 447-466
291. Study of inclusive J / psi production in two photon collisions at LEP-2 with the DELPHI detector, DELPHI Collaboration, Phys.Lett. B565 (2003) 76-86
292. DIRAC: Distributed Infrastructure with Remote Agent Control, A. Tsaregorodtsev, V. Garonne (Marseille, CPPM), J. Closier, M. Frank, C. Gaspar, E. van Herwijnen, F. Loverre, S. Ponce (CERN), R. Graciani Diaz (Barcelona U.), D. Galli (INFN, Bologna) et al.. Jun 2003. 8 pp., eConf C0303241 (2003) TUAT006
293. A Study of the energy evolution of event shape distributions and their means with the DELPHI detector at LEP, DELPHI Collaboration, Eur.Phys.J. C29 (2003) 285-312
294. Search for the standard model Higgs boson at LEP, LEP Working Group for Higgs boson searches and ALEPH and DELPHI and L3 and OPAL Collaboration,s (R. Barate et al.). Mar 2003. 23 pp., Phys.Lett. B565 (2003) 61-75
295. Final results from DELPHI on the searches for SM and MSSM neutral Higgs bosons, DELPHI Collaboration, Eur.Phys.J. C32 (2004) 145-183
296. b tagging in DELPHI at LEP, DELPHI Collaboration, Eur.Phys.J. C32 (2004) 185-208
297. Search for resonant sneutrino production at s**1/2 = 183-GeV to 208-GeV, DELPHI Collaboration, Eur.Phys.J. C28 (2003) 15-26
298. Inclusive b decays to wrong sign charmed mesons, DELPHI Collaboration, Phys.Lett. B561 (2003) 26-40
299. Search for supersymmetric particles in light gravitino scenarios and sleptons NLSP, DELPHI Collaboration, Eur.Phys.J. C27 (2003) 153-172
300. Search for doubly charged Higgs bosons at LEP-2, DELPHI Collaboration, Phys.Lett. B552 (2003) 127-137
301. Search for an LSP gluino at LEP with the DELPHI detector, DELPHI Collaboration, Eur.Phys.J. C26 (2003) 505-525
302. Search for B(s)0 - anti-B(s)0 oscillations and a measurement of B(d)0 - anti-B(d)0 oscillations using events with an inclusively reconstructed vertex, DELPHI Collaboration, Eur.Phys.J. C28 (2003) 155-173
303. Rapidity alignment and p(T) compensation of particle pairs in hadronic Z0 decays, DELPHI Collaboration, Phys.Lett. B533 (2002) 243-252
304. Searches for neutral Higgs bosons in e+ e- collisions from s**(1/2) = 191.6-GeV to 201.7-GeV, DELPHI Collaboration, Eur.Phys.J. C23 (2002) 409-435
305. Search for charged Higgs bosons in e+ e- collisions at s**(1/2) = 189-GeV - 202-GeV, DELPHI Collaboration, Phys.Lett. B525 (2002) 17-28
306. Search for technicolor with DELPHI, DELPHI Collaboration, Eur.Phys.J. C22 (2001) 17-29
307. Single intermediate vector boson production in e+ e- collisions at s**(1/2) = 183-GeV and 189-GeV, DELPHI Collaboration, Phys.Lett. B515 (2001) 238-254
308. Measurement of the semileptonic b branching fractions and average b mixing parameter in Z decays, DELPHI Collaboration, Eur.Phys.J. C20 (2001) 455-478
309. A Measurement of the tau topological branching ratios, DELPHI Collaboration, Eur.Phys.J. C20 (2001) 617-637
310. Measurement of the mass and width of the W boson in e+e− collisions at s√ = 189-GeV, DELPHI Collaboration, Phys.Lett. B511 (2001) 159-177
311. Measurement of V(cb) from the decay process anti-B0 D*+ lepton- anti-neutrino, DELPHI Collaboration, Phys.Lett. B510 (2001) 55-74
312. Search for a fermiophobic Higgs at LEP-2, DELPHI Collaboration, Phys.Lett. B507 (2001) 89-103
313. Measurement of trilinear gauge boson couplings WWV, (V = Z, gamma) in e+ e- collisions at 189-GeV, DELPHI Collaboration, Phys.Lett. B502 (2001) 9-23
314. Update of the search for supersymmetric particles in scenarios with gravitino LSP and sleptons NLSP, DELPHI Collaboration, Phys.Lett. B503 (2001) 34-48
315. Measurement of the B0s lifetime and study of B0s−B¯0s oscillations using Dsℓ events, DELPHI Collaboration, Eur.Phys.J. C16 (2000) 555
316. A Study of the Lorentz structure in tau decays, DELPHI Collaboration, Eur.Phys.J. C16 (2000) 229-252
317. Search for heavy stable and longlived particles in e+ e- collisions at s**(1/2) = 189-GeV, DELPHI Collaboration, Phys.Lett. B478 (2000) 65-72
318. Search for supersymmetric partners of top and bottom quarks at s**(1/2) = 189-GeV, DELPHI Collaboration, Phys.Lett. B496 (2000) 59-75
319. Search for R-parity violation with a anti-U anti-D anti-D coupling at x**(1/2) = 189-GeV, DELPHI Collaboration, Phys.Lett. B500 (2001) 22-36
320. Search for SUSY with R-parity violating LL anti-E couplings at s**(1/2) = 189-GeV, DELPHI Collaboration, Phys.Lett. B487 (2000) 36-52
321. Search for the standard model Higgs boson at LEP in the year 2000, DELPHI Collaboration, Phys.Lett. B499 (2001) 23-37
322. **Search for sleptons in e+ e- collisions at s(1/2) = 183-GeV to 189-GeV, DELPHI Collaboration, Eur.Phys.J. C19 (2001) 29-42**
323. Search for spontaneous R-parity violation at s**(1/2) = 183-GeV and 189-GeV, DELPHI Collaboration, Phys.Lett. B502 (2001) 24-36
324. Study of dimuon production in photon-photon collisions and measurement of QED photon structure functions at LEP, DELPHI Collaboration, Eur.Phys.J. C19 (2001) 15-28
325. Search for neutralino pair production at s**(1/2) = 189-GeV, DELPHI Collaboration, Eur.Phys.J. C19 (2001) 201-212
326. Measurement of the Z Z cross-section in e+ e- interactions at 183-GeV - 189-GeV, DELPHI Collaboration, Phys.Lett. B497 (2001) 199-213
327. Search for the sgoldstino at s**(1/2) from 189-GeV to 202-GeV, DELPHI Collaboration, Phys.Lett. B494 (2000) 203-214
328. Study of B0(S) anti-B0(S) oscillations and B0(S) lifetimes using hadronic decays of B0(S) mesons, DELPHI Collaboration, Eur.Phys.J. C18 (2000) 229-252
329. Limits on the masses of supersymmetric particles at s**(1/2) = 189-GeV, DELPHI Collaboration, Phys.Lett. B489 (2000) 38-54
330. Determination of the e+ e- gamma gamma (gamma) cross-section at centre-of-mass energies ranging from 189-GeV to 202-GeV, DELPHI Collaboration, Phys.Lett. B491 (2000) 67-80
331. Rapidity-rank structure of p anti-p pairs in hadronic Z0 decays, DELPHI Collaboration, Phys.Lett. B490 (2000) 61-70
332. Update of the search for charginos nearly mass-degenerate with the lightest neutralino, DELPHI Collaboration, Phys.Lett. B485 (2000) 95-106
333. Measurement and interpretation of fermion-pair production at LEP energies of 183-GeV and 189-GeV, DELPHI Collaboration, Phys.Lett. B485 (2000) 45-61
334. Searches for neutral Higgs bosons in e+ e- collisions around s**(1/2) = 189-GeV, DELPHI Collaboration, Eur.Phys.J. C17 (2000) 187-205, Addendum-ibid. C17 (2000) 549-551
335. W pair production cross-section and W branching fractions in e+ e- interactions at 189-GeV, DELPHI Collaboration, Phys.Lett. B479 (2000) 89-100
336. Charged and identified particles in the hadronic decay of W bosons and in e+ e- q anti-q from 130-GeV to 200-GeV, DELPHI Collaboration, Eur.Phys.J. C18 (2000) 203-228, Erratum-ibid. C25 (2002) 493
337. Identified charged particles in quark and gluon jets, DELPHI Collaboration, Eur.Phys.J. C17 (2000) 207-222
338. Search for charginos in e+ e- interactions at S**(1/2) = 189-GeV, DELPHI Collaboration, Phys.Lett. B479 (2000) 129-143
339. Inclusive Sigma- and Lambda(1520) production in hadronic Z decays, DELPHI Collaboration, Phys.Lett. B475 (2000) 429-447
340. Hadronization properties of b quarks compared to light quarks in e+ e- to q anti-q from 183-GeV to 200-GeV, DELPHI Collaboration, Phys.Lett. B479 (2000) 118-128, Erratum-ibid. B492 (2000) 398
341. Search for supersymmetric particles in scenarios with a gravitino LSP and stau NLSP, DELPHI Collaboration, Eur.Phys.J. C16 (2000) 211-228
342. Determination of |V(ub)| / |V(cb)| with DELPHI at LEP, DELPHI Collaboration, Phys.Lett. B478 (2000) 14-30
343. Measurement of the anti-B D* pi lepton anti-neutrino(lepton) branching fraction, DELPHI Collaboration, Phys.Lett. B475 (2000) 407-428
344. Lambda(b) polarization in Z0 decays at LEP, DELPHI Collaboration, Phys.Lett. B474 (2000) 205-222
345. Two-dimensional analysis of the Bose-Einstein correlations in e+ e- annihilation at the Z0 peak, DELPHI Collaboration, Phys.Lett. B471 (2000) 460-470
346. Upper limit for the decay B−→τ− anti-neutrino (τ) and measurement of the b→τ anti-neutrino (τ) X branching ratio, DELPHI Collaboration, Phys.Lett. B496 (2000) 43-58
347. A Precise measurement of the tau polarization at LEP-1, DELPHI Collaboration, Eur.Phys.J. C14 (2000) 585-611
348. Measurement of the gluon fragmentation function and a comparison of the scaling violation in gluon and quark jets, DELPHI Collaboration, Eur.Phys.J. C13 (2000) 573-589
349. Measurement of the strange quark forward backward asymmetry around the Z0 peak, DELPHI Collaboration, Eur.Phys.J. C14 (2000) 613-631
350. Search for chargino pair production in scenarios with gravitino LSP and stau NLSP at S**(1/2) approximately equal to 183-GeV at LEP, DELPHI Collaboration, Phys.Lett. B466 (1999) 61-70
351. Multiplicity fluctuations in one-dimensional and two-dimensional angular intervals compared with analytic QCD calculations, DELPHI Collaboration, Phys.Lett. B457 (1999) 368-382
