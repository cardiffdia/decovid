# DECOVID Technical Skills Evidence

Here we provide supplementary information on the Data Innovation Accelerator in support of our DECOVID application.

## DIA Activities

The mission of the [Data Innovation Accelerator](https://www.cardiff.ac.uk/data-innovation-accelerator) is to run collaborative data science projects and data innovation health check reports for SMEs in East Wales. We work across many different industry sectors and have produced data science-related output for 35 companies to date in the region.

As part of our work with SMEs, we provide health check reports that provide a preliminary assessment of the strategic use of data managed by each company. Based on our initial discussions and subsequent studies we provide a set of recommendations to leverage their existing data to generate new opportunities and to bring new products or services to market.

Following the completion of our data innovation survey and subsequent discussions with our data science team, we develop a selected case study and identify practical next steps to provide the company with a blueprint for data innovation.

If you are interested in what we do, we can provide you a copy of an example health check report we prepared for a healthcare SME who specialise in provide digital solutions for patient consent. Please email `dia@cardiff.ac.uk` and we'll send you a copy. Please note this should only be used as evidence and not be redistributed.

## Data science demonstrations

We provide annotated demonstrations of key data science techniques as part of our selected case studies and collaborative projects agreed with each company. These techniques incorporate either real-world company data or adaptation of publicly available data. We pitch the explanations in the demonstrations to be understandable to a non-technical audience

Some examples of our annotated demonstrations are provided below:

- [Channel Attribution Modelling for Patient Journeys](notebooks/patient_journeys.ipynb)
- [Predicting Patient Outcomes following Surgery: A Literature Review](other/review-ml-epi.md)
- [Predictive Analysis](notebooks/predictive-analysis.ipynb)
- [Investigating Site Characteristics Relationship with Factor Analysis](notebooks/house_price_fa.ipynb)
- [Agglomerative Hierarchical Clustering](notebooks/clustering.ipynb)
- [Time Series Forecasting using SARIMA](notebooks/SARIMA-Forecasting.ipynb)
- [Recommender Systems](notebooks/recommender_systems.ipynb)
- [Route Optimisation using Simulated Annealing](notebooks/simulated_annealing.ipynb)
- [Synthetic data with Kernel Density Estimation](notebooks/kde_synthetic.ipynb)

## DIA Team Expertise

In our first year of operations, we have helped companies maximise the value of their data by applying a wide range of data science techniques including:

- Predictive Modelling (time series forecasting)
- Clustering and factor analysis
- Classification and multivariate regression
- Entity-relationship modelling (association rule mining, graph analysis)
- Markov chains
- Recommender systems
- Logistics optimisation
- Advanced visualisation techniques and interactive dashboards
- Synthetic data generation for GDPR-compliant counterfactual analysis
- Natural Language Processing (e.g sentiment analysis, text similarity, topic modelling, named entity recognition)
- Big Data Mining
- Stream Data Processing and Analytics
- Social Media Analytics
- Bespoke virtual assistants and chatbot development
- Image processing using CNNs
- Real-Time Adaptive Intelligence Systems

Other areas of data science expertise in the team include:
- Cancer survival analysis
- Patient Reported Outcomes/Experience Measures analysis (PROMS/PREMS)
- Intervention analysis
- Missing data and imputation techniques

***

## Background of the DIA Team

All of the DIA data science team have an expressed an interest in contributed to DECOVID work.

### Andrew Washbrook

Andrew received his PhD in experimental Particle Physics from the University of Liverpool and stayed in the field to specialise in operations and development of global-scale data storage and processing.

As part of his time working at the Large Hadron Collider (LHC) he held a number of prominent roles in distributed computing and sustainable software coordination. His research interests have explored the application of emerging hardware and systems for extreme velocity data processing, GPU-based algorithm acceleration and the use of supercomputing facilities for materials simulation.

**Specialisms**: Big Data, Systems development, Data analysis

**Papers**
- [List of papers related to work in High Energy Physics](other/aw-papers.md)

### Ceri White

Ceri received his PhD from University of Glamorgan’s School of Computer Science in 2009 and worked as a statistician in the National Health Service for over fifteen years in population health intelligence.

His PhD research concerned small area data analysis in terms of clustering and modelling the population at risk for particular hazards such as electric power lines and landfill sites.

**Specialisms**: Statistics, Data mining, Business Intelligence (BI)

**Thesis**
- Cluster analysis: Algorithms, Hazards and Small Area Relative Survival

**Relevant Publications**
- Impact of neuroendocrine morphology on cancer outcomes and stage at diagnosis: a UK nationwide cohort study 2013–2015, Tracey S. E. Genus, Catherine Bouvier, Kwok F. Wong, Rajaventhan Srirajaskanthan, Brian A. Rous, Denis C. Talbot, Juan W. Valle, Mohid Khan, Neil Pearce, Mona Elshafie, Nicholas S. Reed, Eileen Morgan, Andrew Deas, Ceri White, Dyfed Huws & John Ramage, British Journal of Cancer
volume 121, pages 966–972 (2019) https://www.nature.com/articles/s41416-019-0606-3

- The fraction of cancer attributable to modifiable risk factors in England, Wales, Scotland, Northern Ireland, and the United Kingdom in 2015, Katrina F. Brown, Harriet Rumgay, Casey Dunlop, Margaret Ryan, Frances Quartly, Alison Cox, Andrew Deas, Lucy Elliss-Brookes, Anna Gavin, Luke Hounsome, Dyfed Huws, Nick Ormiston-Smith, Jon Shelton, Ceri White & D. Max Parkin, British Journal of Cancer Volume 118, pages 1130–1141(2018) October 2018 https://www.nature.com/articles/s41416-018-0029-6

### Gillian Baird

Gillian received her MSc from the University of St Andrews in Applied Statistics and Data mining in 2017. Her MSc. thesis focused on the use of time series analysis techniques to model market risk estimations, during extreme financial market conditions.

Before joining the DIA, Gillian worked as the lead data scientist and data engineer at Felcana; a start-up tech company based in London. There, she developed the Activity Recognition models used in the company’s flagship IoT device and mobile application and engineered their cloud-based data and security infrastructures.

**Specialisms**: Statistics, Cloud Engineering, Data Mining

**Thesis**
- An empirical and theoretical comparison of the performance of popular volatility models (Historical Simulations, RiskMetrics®, GARCH) to predict Value-at-Risk and Expected Tail Loss forecasts for various financial indexes, under different market conditions.

### Emily Elias

Emily received her BSc in Internet Computing and Systems Administration from Aberystwyth University in 2017, and an MSc in Data Science from Swansea University in 2018.

Her MSc thesis was around the area of text mining and involved using machine learning algorithms such as Support Vector Machines and Latent Dirichlet Allocation to detect similarities, topics and any predictive features in university educational material.

**Specialisms**: Text Mining, Natural Language Processing, Data Visualisation

### Sachin Galahitiya

Sachin is a graduate of Cardiff University with an MSc in Data Science and Analytics.  Having completed his BEng in Software Engineering from Staffordshire University, he worked as a software developer in the supply chain industry for three years. His MSc thesis was around the area of human action recognition using evidential deep learning to compute classification uncertainty.

**Specialisms**: Computer Vision, Data Visualisation, Statistics

**Thesis**
- Evidential deep learning to compute uncertainty measures for video classification

**Showcases**
 - [Why is UK Falling behind the electric car race?](https://s4n10.github.io/)

### Michael Holmes

Michael received his PhD from Manchester Metropolitan University's Intelligent Systems Group in 2017 and worked as a post-doctorate research associate in machine learning with the IRC-SPHERE project at the University of Bristol.

His research explored the use of machine learning algorithms for near real-time analysis of non-verbal behaviour in an educational setting. His recent work has focused on data mining and machine learning for IoT sensor network data in-home healthcare.

**Specialisms**: Stream Data Processing and Analytics, Computer Vision and Image Processing, Real-time and Adaptive Intelligent Systems

**Relevant Work**
- He worked with very large volumes of sensor data collected from real-world installations of the SPHERE sensor network deployed in peoples' homes. One cohort being healthy volunteers (SPHERE 100-Homes) and a second cohort (HEmiSPHERE) of patients in recovery from total Hip or Knee replacement surgery.

**Relevant Publications**
- Holmes M. et al., ‘Modelling patient behaviour using IoT sensor data: A case study to evaluate techniques for modelling domestic behaviour in recovery from total Hip replacement surgery’, Journal of Healthcare Informatics Research, Springer, 2020.
- A. Elsts et al., ‘A Guide to the SPHERE 100 Homes Study Dataset’, arXiv:1805.11907 [cs], Oct. 2018, Accessed: Apr. 09, 2020. [Online]. Available: http://arxiv.org/abs/1805.11907.
- Holmes M. et al., ‘Analysis of patient domestic activity in recovery from Hip or Knee replacement surgery: modelling wrist-worn wearable RSSI and Accelerometer data in the wild’, Proceedings of the 3rd International Workshop on Knowledge Discovery in Healthcare @ IJCAI, 2018.
- Diethe T. et al., ‘Releasing eHealth Analytics into the Wild: Lessons Learnt from the SPHERE Project’, Proceedings of the 24th ACM SIGKDD Conference on Knowledge Discovery and Data Mining (KDD), 2018.

### Dimitra Mavridou

Dimitra is a graduate of Cardiff University with an MSc in Data Science and Analytics in 2018, while her undergraduate degree was in Production Engineering in 2015.

For her MSc thesis, she studied the processing and archiving large datasets using the Hierarchical Data Format (HDF5).

**Specialisms**: Statistics, Data Visualisation, Big Data

**Theses**
- Tracking Retail Centre visits using Mobile Device Detection
- Comparative performance evaluation of production control policies with discrete-event simulation models

### Lowri Williams

Lowri received her PhD from Cardiff University’s School of Computer Science & Informatics in 2017.

She has collaborated with multidisciplinary experts from Cardiff, Swansea, Bangor, and Lancaster University to compile a collection of ten million words of spoken, written, and digital Welsh language discourse. Lowri has also contributed to other research fields, including cybersecurity.

**Specialisms**: Natural Language Processing, Sentiment Analysis, Data Mining

**Publications**
- Available on [Google Scholar](https://scholar.google.co.uk/citations?user=yCk02XcAAAAJ&hl=en&oi=ao)

**Thesis**
- Pushing the envelope of sentiment analysis beyond words and polarities

**Other**
- [Public repositories](https://github.com/LowriWilliams?tab=repositories)
- [Personal Blog (Coronavirus)](https://lowriwilliams.github.io/projects/coronavirus.html)
- Data Analysis with US pharmaceutical data as part of [Codementor](https://www.codementor.io/@lowriawilliams) work
